<!DOCTYPE html>

<!--[if IE 8]> <html lang="en" class="ie8 no-js"> <![endif]-->
<!--[if IE 9]> <html lang="en" class="ie9 no-js"> <![endif]-->
<!--[if !IE]><!-->
<html lang="en">
<!--<![endif]-->
<!-- Begin Head -->

<head>
    <title>Miraculous - Online Music Store Html Template</title>
    <meta charset="utf-8">
    <meta content="width=device-width, initial-scale=1.0" name="viewport">
    <meta name="description" content="Music">
    <meta name="keywords" content="">
    <meta name="author" content="kamleshyadav">
    <meta name="MobileOptimized" content="320">
    <!--Start Style -->

    <link rel="stylesheet" type="text/css" href="/assets/css/fonts.css">
    <link rel="stylesheet" type="text/css" href="/assets/css/bootstrap.css">
    <link rel="stylesheet" type="text/css" href="/assets/css/font-awesome.min.css">


	<link rel="stylesheet" type="text/css" href="/assets/js/plugins/scroll/jquery.mCustomScrollbar.css">

    <link rel="stylesheet" type="text/css" href="/assets/css/style.css">
    <!-- Favicon Link -->
    <link rel="shortcut icon" type="image/png" href="images/favicon.png">
</head>

<body>
	<!----Loader Start---->
    <div class="ms_inner_loader">
        <div class="ms_loader">
            <div class="ms_bars">
                <div class="bar"></div>
                <div class="bar"></div>
                <div class="bar"></div>
                <div class="bar"></div>
                <div class="bar"></div>
                <div class="bar"></div>
                <div class="bar"></div>
                <div class="bar"></div>
                <div class="bar"></div>
                <div class="bar"></div>
            </div>
        </div>
    </div>
    <!----Main Wrapper Start---->
    <div id="boranpa"></div>


    <script type="text/javascript" src="{{asset('js/site/library.js')}}"></script>
    <script type="text/javascript" src="/assets/js/plugins/swiper/js/swiper.min.js"></script>
    <script type="text/javascript" src="/assets/js/plugins/scroll/jquery.mCustomScrollbar.js"></script>
    <script type="text/javascript" src="/assets/js/custom.js"></script>

    <script type="text/javascript" src="{{mix('js/site/app.js')}}"></script>
</body>

</html>

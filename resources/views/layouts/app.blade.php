<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="apple-touch-icon" sizes="76x76" href="/vendor/assets/img/apple-icon.png">
    <link rel="icon" type="image/png" sizes="96x96" href="/vendor/assets/img/favicon.png">

    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>@yield('title')</title>
    <meta content='width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0' name='viewport' />
    <link rel="stylesheet" type="text/css" href="https://fonts.googleapis.com/css?family=Roboto:300,400,500,700|Roboto+Slab:400,700|Material+Icons" />
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/latest/css/font-awesome.min.css">

    <link rel="stylesheet" type="text/css" href="{{asset('assets/css/fonts.css')}}">
    <link rel="stylesheet" type="text/css" href="{{asset('assets/css/bootstrap.css')}}">
    <link rel="stylesheet" type="text/css" href="{{asset('assets/css/font-awesome.min.css')}}">


    <link rel="stylesheet" type="text/css" href="/assets/js/plugins/scroll/jquery.mCustomScrollbar.css">

    <link rel="stylesheet" type="text/css" href="/assets/css/style.css">

    <script>
        window.userBoranpa = {!! json_encode([
     'user' => auth()->user(),
     'roles' => auth()->guest() ? : auth()->user()->roles,
     'permissions' => auth()->guest() ? : auth()->user()->getAllPermissions(),
     'guest' => auth()->guest(),
     'url_site' => htmlspecialchars(config('app.url')),
     'name_site' => htmlspecialchars(config('app.name')),
    ]) !!}
    </script>
</head>
<body>
<!----Loader Start---->
<div class="ms_inner_loader">
    <div class="ms_loader">
        <div class="ms_bars">
            <div class="bar"></div>
            <div class="bar"></div>
            <div class="bar"></div>
            <div class="bar"></div>
            <div class="bar"></div>
            <div class="bar"></div>
            <div class="bar"></div>
            <div class="bar"></div>
            <div class="bar"></div>
            <div class="bar"></div>
        </div>
    </div>
</div>
<!----Main Wrapper Start---->

@yield('content')

@routes

<script type="text/javascript" src="{{asset('js/site/library.js')}}"></script>
<script type="text/javascript" src="/assets/js/plugins/swiper/js/swiper.min.js"></script>
<script type="text/javascript" src="/assets/js/plugins/scroll/jquery.mCustomScrollbar.js"></script>
<script type="text/javascript" src="/assets/js/custom.js"></script>

<script type="text/javascript" src="{{mix('js/site/app.js')}}"></script>
</body>
</html>

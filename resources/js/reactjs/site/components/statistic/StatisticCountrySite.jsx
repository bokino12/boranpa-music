import React, {Component} from 'react';
import HelmetSite from "../inc/vendor/HelmetSite";
import VerticalNaveSite from "../inc/VerticalNaveSite";
import HorizontaNaveSite from "../inc/HorizontaNaveSite";
import {Link} from "react-router-dom";
import FooterMini from "../inc/FooterMini";
import FooterMultiple from "../inc/FooterMultiple";
import UndoRedo from "../inc/vendor/UndoRedo";

class StatisticCountrySite extends Component {
    render() {
        return (
            <>
                <HelmetSite title={`Statistic Cameroon `}/>

                <div className="ms_main_wrapper">

                    <VerticalNaveSite/>

                    <div className="ms_content_wrapper padder_top90">

                        <HorizontaNaveSite/>


                        { /**Banner**/}
                        <div className="ms-banner">

                            {/** Weekly Top 15**/}
                            <div className="ms_weekly_wrapper">
                                <div className="ms_weekly_inner">

                                    <UndoRedo {...this.props}/>

                                    <div className="row">
                                        <div className="col-lg-12">
                                            <div className="ms_heading">
                                                <h1>weekly top 10</h1>
                                            </div>
                                        </div>
                                        <div className="col-lg-4 col-md-12 padding_right40">
                                            <div className="ms_weekly_box">
                                                <div className="weekly_left">
                                    <span className="w_top_no">
										01
									</span>
                                                    <div className="w_top_song">
                                                        <div className="w_tp_song_img">
                                                            <img src="/assets/images/weekly/song1.jpg" alt=""
                                                                 className="img-fluid"/>
                                                            <div className="ms_song_overlay">
                                                            </div>
                                                            <div className="ms_play_icon">
                                                                <img src="/assets/images/svg/play.svg" alt=""/>
                                                            </div>
                                                        </div>
                                                        <div className="w_tp_song_name">
                                                            <h3><a href="#">Until I Met You</a></h3>
                                                            <p>Ava Cornish</p>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div className="weekly_right">
                                                    <span className="w_song_time">3:10</span>
                                                    <span className="w_song_time" data-other="1">2 987 6543</span>
                                                </div>
                                            </div>
                                            <div className="ms_divider"></div>
                                            <div className="ms_weekly_box">
                                                <div className="weekly_left">
                                    <span className="w_top_no">
										02
									</span>
                                                    <div className="w_top_song">
                                                        <div className="w_tp_song_img">
                                                            <img src="/assets/images/weekly/song2.jpg" alt=""
                                                                 className="img-fluid"/>
                                                            <div className="ms_song_overlay">
                                                            </div>
                                                            <div className="ms_play_icon">
                                                                <img src="/assets/images/svg/play.svg" alt=""/>
                                                            </div>
                                                        </div>
                                                        <div className="w_tp_song_name">
                                                            <h3><a href="#">Walking Promises</a></h3>
                                                            <p>Ava Cornish</p>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div className="weekly_right">
                                                    <span className="w_song_time">5:10</span>
                                                    <span className="w_song_time" data-other="1">1 000 6543</span>
                                                </div>
                                            </div>
                                            <div className="ms_divider"></div>
                                            <div className="ms_weekly_box">
                                                <div className="weekly_left">
                                    <span className="w_top_no">
										03
									</span>
                                                    <div className="w_top_song">
                                                        <div className="w_tp_song_img">
                                                            <img src="/assets/images/weekly/song3.jpg" alt=""
                                                                 className="img-fluid"/>
                                                            <div className="ms_song_overlay">
                                                            </div>
                                                            <div className="ms_play_icon">
                                                                <img src="/assets/images/svg/play.svg" alt=""/>
                                                            </div>
                                                        </div>
                                                        <div className="w_tp_song_name">
                                                            <h3><a href="#">Gimme Some Courage</a></h3>
                                                            <p>Ava Cornish</p>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div className="weekly_right">
                                                    <span className="w_song_time">5:10</span>
                                                    <span className="w_song_time" data-other="1">2309</span>
                                                </div>
                                            </div>
                                            <div className="ms_divider"></div>
                                            <div className="ms_weekly_box">
                                                <div className="weekly_left">
                                    <span className="w_top_no">
										04
									</span>
                                                    <div className="w_top_song">
                                                        <div className="w_tp_song_img">
                                                            <img src="/assets/images/weekly/song4.jpg" alt=""
                                                                 className="img-fluid"/>
                                                            <div className="ms_song_overlay">
                                                            </div>
                                                            <div className="ms_play_icon">
                                                                <img src="/assets/images/svg/play.svg" alt=""/>
                                                            </div>
                                                        </div>
                                                        <div className="w_tp_song_name">
                                                            <h3><a href="#">Desired Games</a></h3>
                                                            <p>Ava Cornish</p>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div className="weekly_right">
                                                    <span className="w_song_time">5:10</span>
                                                    <span className="w_song_time" data-other="1">655 6543</span>
                                                </div>
                                            </div>
                                            <div className="ms_divider"></div>
                                            <div className="ms_weekly_box">
                                                <div className="weekly_left">
                                    <span className="w_top_no">
										05
									</span>
                                                    <div className="w_top_song">
                                                        <div className="w_tp_song_img">
                                                            <img src="/assets/images/weekly/song5.jpg" alt=""
                                                                 className="img-fluid"/>
                                                            <div className="ms_song_overlay">
                                                            </div>
                                                            <div className="ms_play_icon">
                                                                <img src="/assets/images/svg/play.svg" alt=""/>
                                                            </div>
                                                        </div>
                                                        <div className="w_tp_song_name">
                                                            <h3><a href="#">Dark Alley Acoustic</a></h3>
                                                            <p>Ava Cornish</p>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div className="weekly_right">
                                                    <span className="w_song_time">4:10</span>
                                                    <span className="w_song_time" data-other="1">508 6543</span>
                                                </div>
                                            </div>
                                        </div>
                                        <div className="col-lg-4 col-md-12 padding_right40">
                                            <div className="ms_weekly_box">
                                                <div className="weekly_left">
                                    <span className="w_top_no">
										06
									</span>
                                                    <div className="w_top_song">
                                                        <div className="w_tp_song_img">
                                                            <img src="/assets/images/weekly/song6.jpg" alt=""
                                                                 className="img-fluid"/>
                                                            <div className="ms_song_overlay">
                                                            </div>
                                                            <div className="ms_play_icon">
                                                                <img src="/assets/images/svg/play.svg" alt=""/>
                                                            </div>
                                                        </div>
                                                        <div className="w_tp_song_name">
                                                            <h3><a href="#">Walking Promises</a></h3>
                                                            <p>Ava Cornish</p>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div className="weekly_right">
                                                    <span className="w_song_time">4:10</span>
                                                    <span className="w_song_time" data-other="1">409 6543</span>
                                                </div>
                                            </div>
                                            <div className="ms_divider"></div>
                                            <div className="ms_weekly_box">
                                                <div className="weekly_left">
                                    <span className="w_top_no">
										07
									</span>
                                                    <div className="w_top_song">
                                                        <div className="w_tp_song_img">
                                                            <img src="/assets/images/weekly/song7.jpg" alt=""
                                                                 className="img-fluid"/>
                                                            <div className="ms_song_overlay">
                                                            </div>
                                                            <div className="ms_play_icon">
                                                                <img src="/assets/images/svg/play.svg" alt=""/>
                                                            </div>
                                                        </div>
                                                        <div className="w_tp_song_name">
                                                            <h3><a href="#">Endless Things</a></h3>
                                                            <p>Ava Cornish</p>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div className="weekly_right">
                                                    <span className="w_song_time">8:10</span>
                                                    <span className="w_song_time" data-other="1">244 6543</span>
                                                </div>
                                            </div>
                                            <div className="ms_divider"></div>
                                            <div className="ms_weekly_box ms_active_play">
                                                <div className="weekly_left">
                                    <span className="w_top_no">
										08
									</span>
                                                    <div className="w_top_song">
                                                        <div className="w_tp_song_img">
                                                            <img src="/assets/images/weekly/song8.jpg" alt=""
                                                                 className="img-fluid"/>
                                                            <div className="ms_song_overlay">
                                                            </div>
                                                            <div className="ms_play_icon">
                                                                <div className="ms_bars">
                                                                    <div className="bar"></div>
                                                                    <div className="bar"></div>
                                                                    <div className="bar"></div>
                                                                    <div className="bar"></div>
                                                                    <div className="bar"></div>
                                                                    <div className="bar"></div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div className="w_tp_song_name">
                                                            <h3><a href="#">Dream Your Moments</a></h3>
                                                            <p>Ava Cornish</p>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div className="weekly_right">
                                                    <span className="w_song_time">4:10</span>
                                                    <span className="w_song_time" data-other="1">231 6543</span>
                                                </div>
                                            </div>
                                            <div className="ms_divider"></div>
                                            <div className="ms_weekly_box">
                                                <div className="weekly_left">
                                    <span className="w_top_no">
										09
									</span>
                                                    <div className="w_top_song">
                                                        <div className="w_tp_song_img">
                                                            <img src="/assets/images/weekly/song9.jpg" alt=""
                                                                 className="img-fluid"/>
                                                            <div className="ms_song_overlay">
                                                            </div>
                                                            <div className="ms_play_icon">
                                                                <img src="/assets/images/svg/play.svg" alt=""/>
                                                            </div>
                                                        </div>
                                                        <div className="w_tp_song_name">
                                                            <h3><a href="#">Until I Met You</a></h3>
                                                            <p>Ava Cornish</p>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div className="weekly_right">
                                                    <span className="w_song_time">4:10</span>
                                                    <span className="w_song_time" data-other="1">133 6543</span>
                                                </div>
                                            </div>
                                            <div className="ms_divider"></div>
                                            <div className="ms_weekly_box">
                                                <div className="weekly_left">
                                    <span className="w_top_no">
										10
									</span>
                                                    <div className="w_top_song">
                                                        <div className="w_tp_song_img">
                                                            <img src="/assets/images/weekly/song5.jpg" alt=""
                                                                 className="img-fluid"/>
                                                            <div className="ms_song_overlay">
                                                            </div>
                                                            <div className="ms_play_icon">
                                                                <img src="/assets/images/svg/play.svg" alt=""/>
                                                            </div>
                                                        </div>
                                                        <div className="w_tp_song_name">
                                                            <h3><a href="#">Gimme Some Courage</a></h3>
                                                            <p>Ava Cornish</p>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div className="weekly_right">
                                                    <span className="w_song_time">4:10</span>
                                                    <span className="w_song_time" data-other="1">9 6543</span>
                                                </div>
                                            </div>
                                        </div>
                                        <div className="col-lg-4 col-md-12">
                                            <div className="ms_weekly_box">
                                                <div className="weekly_left">
                                    <span className="w_top_no">
										11
									</span>
                                                    <div className="w_top_song">
                                                        <div className="w_tp_song_img">
                                                            <img src="/assets/images/weekly/song2.jpg" alt=""
                                                                 className="img-fluid"/>
                                                            <div className="ms_song_overlay">
                                                            </div>
                                                            <div className="ms_play_icon">
                                                                <img src="/assets/images/svg/play.svg" alt=""/>
                                                            </div>
                                                        </div>
                                                        <div className="w_tp_song_name">
                                                            <h3><a href="#">Dark Alley Acoustic</a></h3>
                                                            <p>Ava Cornish</p>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div className="weekly_right">
                                                    <span className="w_song_time">9:10</span>
                                                    <span className="w_song_time" data-other="1">8 6543</span>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            {/**Featured Albumn Section Start**/}
                            <div className="ms_fea_album_slider">
                                <div className="ms_heading">
                                    <h1>Top Track</h1>
                                    <span className="veiw_all"><a href="#">view more</a></span>
                                </div>
                                <div className="row">
                                    <div className="col-md-3">
                                        <div className="ms_rcnt_box">
                                            <div className="ms_rcnt_box_img">
                                                <Link to={`/albums/show/`}>
                                                    <img src="/assets/images/album/album1.jpg" alt=""/>
                                                </Link>
                                                <div className="ms_main_overlay">
                                                    <div className="ms_box_overlay"></div>
                                                    <a href="#">
                                                        <div className="ms_more_icon">
                                                            <img src="/assets/images/svg/more.svg" alt=""/>
                                                        </div>
                                                    </a>
                                                    <div className="ms_play_icon">
                                                        <Link to={`/albums/show/`}><img src="/assets/images/svg/play.svg" alt=""/></Link>
                                                    </div>
                                                </div>
                                            </div>
                                            <div className="ms_rcnt_box_text">
                                                <h3>
                                                    <Link to={`/albums/show/`}>Bloodlust</Link>
                                                </h3>
                                                <p>Ava Cornish & Brian Hill</p>
                                            </div>
                                        </div>
                                    </div>
                                    <div className="col-md-3">
                                        <div className="ms_rcnt_box">
                                            <div className="ms_rcnt_box_img">
                                                <img src="/assets/images/album/album2.jpg" alt=""/>
                                                <div className="ms_main_overlay">
                                                    <div className="ms_box_overlay"></div>
                                                    <a href="#">
                                                        <div className="ms_more_icon">
                                                            <i className="fa fa-eye"/>
                                                        </div>
                                                    </a>
                                                    <div className="ms_play_icon">
                                                        <a href="#"><img src="/assets/images/svg/play.svg" alt=""/></a>
                                                    </div>
                                                </div>
                                            </div>
                                            <div className="ms_rcnt_box_text">
                                                <h3><a href="#">Time flies</a></h3>
                                                <p>Ava Cornish & Brian Hill</p>
                                            </div>
                                        </div>
                                    </div>
                                    <div className="col-md-3">
                                        <div className="ms_rcnt_box">
                                            <div className="ms_rcnt_box_img">
                                                <img src="/assets/images/album/album3.jpg" alt=""/>
                                                <div className="ms_main_overlay">
                                                    <div className="ms_box_overlay"></div>
                                                    <div className="ms_play_icon">
                                                        <a href="#"><img src="/assets/images/svg/play.svg" alt=""/></a>
                                                    </div>
                                                </div>
                                            </div>
                                            <div className="ms_rcnt_box_text">
                                                <h3><a href="#">Dark matters</a></h3>
                                                <p>Ava Cornish & Brian Hill</p>
                                            </div>
                                        </div>
                                    </div>
                                    <div className="col-md-3">
                                        <div className="ms_rcnt_box">
                                            <div className="ms_rcnt_box_img">
                                                <img src="/assets/images/album/album4.jpg" alt=""/>
                                                <div className="ms_main_overlay">
                                                    <div className="ms_box_overlay"></div>
                                                    <div className="ms_play_icon">
                                                        <a href="#"><img src="/assets/images/svg/play.svg" alt=""/></a>
                                                    </div>
                                                </div>
                                            </div>
                                            <div className="ms_rcnt_box_text">
                                                <h3><a href="#">Eye to eye</a></h3>
                                                <p>Ava Cornish & Brian Hill</p>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <FooterMini/>

                        { /***Audio Player Section**/}



                    </div>

                </div>

                <FooterMultiple/>

            </>
        );
    }
}

export default StatisticCountrySite;

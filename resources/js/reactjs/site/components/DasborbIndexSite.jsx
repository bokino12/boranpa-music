import React, {Component} from 'react';
import HelmetSite from './inc/vendor/HelmetSite';
import VerticalNaveSite from "./inc/VerticalNaveSite";
import HorizontaNaveSite from "./inc/HorizontaNaveSite";
import FooterMultiple from "./inc/FooterMultiple";
import SwiperCore from 'swiper';
import { Swiper, SwiperSlide, } from 'swiper/react';
import 'swiper/swiper-bundle.css';
import {Link} from "react-router-dom";
import FooterMini from "./inc/FooterMini";
import GenreInteresse from "./genres/GenreInteresse";
import StationradioInteresse from "./stationradio/StationradioInteresse";
import AlbumInteresse from "./albums/AlbumInteresse";

export class DasborbIndexSite extends Component {

    render() {
        return (
            <>
                <HelmetSite title={`Welcome `}/>

                <div className="ms_main_wrapper">

                    <VerticalNaveSite/>

                    <div className="ms_content_wrapper padder_top80">

                        <HorizontaNaveSite/>


                       { /**Banner**/}
                        <div className="ms-banner">
                            <div className="container-fluid">
                                <div className="row">
                                    <div className="col-lg-12 col-md-12">
                                        <div className="ms_banner_img">
                                            <img src="/assets/images/banner.png" alt="" className="img-fluid"/>
                                        </div>
                                        <div className="ms_banner_text">
                                            <h1>This Month’s</h1>
                                            <h1 className="ms_color">Record Breaking Albums !</h1>
                                            <p>Dream your moments, Until I Met You, Gimme Some Courage, Dark Alley, One
                                                More Of A Stranger, Endless<br/> Things, The Heartbeat Stops, Walking
                                                Promises, Desired Games and many more...</p>
                                            <div className="ms_banner_btn">
                                                <a href="#" className="ms_btn">Listen Now</a>
                                                <a href="#" className="ms_btn">Add To Queue</a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        {/** Weekly Top 15**/}
                            <div className="ms_weekly_wrapper ms_free_music">
                                <div className="ms_weekly_inner">
                                    <div className="row">
                                        <div className="col-lg-12">
                                            <div className="ms_heading">
                                                <h1>Download Trending Tracks</h1>
                                            </div>
                                        </div>
                                        <div className="col-lg-4 col-md-12 padding_right40">
                                            <div className="ms_divider"></div>
                                            <div className="ms_divider"></div>
                                            <div className="ms_weekly_box ms_active_play">
                                                <div className="weekly_left">
                                                    <div className="w_top_song">
                                                        <div className="w_tp_song_img">
                                                            <img src="/assets/images/weekly/song8.jpg" alt=""/>
                                                                <div className="ms_song_overlay">
                                                                </div>
                                                                <div className="ms_play_icon">
                                                                    <div className="ms_bars">
                                                                        <div className="bar"></div>
                                                                        <div className="bar"></div>
                                                                        <div className="bar"></div>
                                                                        <div className="bar"></div>
                                                                        <div className="bar"></div>
                                                                        <div className="bar"></div>
                                                                    </div>
                                                                </div>
                                                        </div>
                                                        <div className="w_tp_song_name">
                                                            <h3><a href="#">Dream Your Moments</a></h3>
                                                            <p>Ava Cornish</p>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div className="weekly_right">
                                                    <span className="w_song_time">5:10</span>
                                                    <a href="#">
                                                        <span className="w_song_dwnload">
                                                            <i className="ms_icon1 dwnload_icon"></i>
                                                        </span>
                                                    </a>
                                                </div>
                                            </div>
                                            <div className="ms_divider"></div>
                                        </div>
                                        <div className="col-lg-4 col-md-12">
                                            <div className="ms_weekly_box">
                                                <div className="weekly_left">
                                                    <div className="w_top_song">
                                                        <div className="w_tp_song_img">
                                                            <img src="/assets/images/weekly/song2.jpg" alt=""/>
                                                                <div className="ms_song_overlay">
                                                                </div>
                                                                <div className="ms_play_icon">
                                                                    <img src="/assets/images/svg/play.svg" alt=""/>
                                                                </div>
                                                        </div>
                                                        <div className="w_tp_song_name">
                                                            <h3><a href="#">Dark Alley Acoustic</a></h3>
                                                            <p>Ava Cornish</p>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div className="weekly_right">
                                                    <span className="w_song_time">5:10</span>
                                                    <a href="#">
                                                        <span className="w_song_dwnload">
                                                            <i className="ms_icon1 dwnload_icon"></i>
                                                        </span>
                                                    </a>
                                                </div>
                                            </div>
                                            <div className="ms_divider"></div>
                                            <div className="ms_weekly_box">
                                                <div className="weekly_left">
                                                    <div className="w_top_song">
                                                        <div className="w_tp_song_img">
                                                            <img src="/assets/images/weekly/song11.jpg" alt=""/>
                                                                <div className="ms_song_overlay">
                                                                </div>
                                                                <div className="ms_play_icon">
                                                                    <img src="/assets/images/svg/play.svg" alt=""/>
                                                                </div>
                                                        </div>
                                                        <div className="w_tp_song_name">
                                                            <h3><a href="#">The Heartbeat Stops</a></h3>
                                                            <p>Ava Cornish</p>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div className="weekly_right">
                                                    <span className="w_song_time">5:10</span>
                                                    <a href="#">
                                                        <span className="w_song_dwnload">
                                                        <i className="ms_icon1 dwnload_icon"></i>
                                                    </span>
                                                    </a>
                                                </div>
                                            </div>
                                            <div className="ms_divider"></div>
                                            <div className="ms_weekly_box">
                                                <div className="weekly_left">
                                                    <div className="w_top_song">
                                                        <div className="w_tp_song_img">
                                                            <img src="/assets/images/weekly/song12.jpg" alt=""/>
                                                                <div className="ms_song_overlay">
                                                                </div>
                                                                <div className="ms_play_icon">
                                                                    <img src="/assets/images/svg/play.svg" alt=""/>
                                                                </div>
                                                        </div>
                                                        <div className="w_tp_song_name">
                                                            <h3><a href="#">One More Stranger</a></h3>
                                                            <p>Ava Cornish</p>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div className="weekly_right">
                                                    <span className="w_song_time">5:10</span>
                                                    <a href="#">
                                                        <span className="w_song_dwnload">
                                                        <i className="ms_icon1 dwnload_icon"></i>
                                                    </span>
                                                    </a>
                                                </div>
                                            </div>
                                            <div className="ms_divider"></div>
                                            <div className="ms_weekly_box">
                                                <div className="weekly_left">
                                                    <div className="w_top_song">
                                                        <div className="w_tp_song_img">
                                                            <img src="/assets/images/weekly/song13.jpg" alt=""/>
                                                                <div className="ms_song_overlay">
                                                                </div>
                                                                <div className="ms_play_icon">
                                                                    <img src="/assets/images/svg/play.svg" alt=""/>
                                                                </div>
                                                        </div>
                                                        <div className="w_tp_song_name">
                                                            <h3><a href="#">Walking Promises</a></h3>
                                                            <p>Ava Cornish</p>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div className="weekly_right">
                                                    <span className="w_song_time">5:10</span>
                                                    <a href="#">
                                                        <span className="w_song_dwnload">
                                                        <i className="ms_icon1 dwnload_icon"></i>
                                                    </span>
                                                    </a>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>

                        <div className="ms_weekly_wrapper">
                            <div className="ms_weekly_inner">
                                <div className="row">
                                    <div className="col-lg-12">
                                        <div className="ms_heading">
                                            <h1>weekly top 15</h1>
                                            <span className="veiw_all"><Link to={`/statistics/country/`}>view more</Link></span>
                                        </div>
                                    </div>
                                    <div className="col-lg-4 col-md-12 padding_right40">
                                        <div className="ms_weekly_box">
                                            <div className="weekly_left">
                                    <span className="w_top_no">
										01
									</span>
                                                <div className="w_top_song">
                                                    <div className="w_tp_song_img">
                                                        <img src="/assets/images/weekly/song1.jpg" alt=""
                                                             className="img-fluid"/>
                                                        <div className="ms_song_overlay">
                                                        </div>
                                                        <div className="ms_play_icon">
                                                            <img src="/assets/images/svg/play.svg" alt=""/>
                                                        </div>
                                                    </div>
                                                    <div className="w_tp_song_name">
                                                        <h3><a href="#">Until I Met You</a></h3>
                                                        <p>Ava Cornish</p>
                                                    </div>
                                                </div>
                                            </div>
                                            <div className="weekly_right">
                                                <span className="w_song_time">5:10</span>
                                                <span className="ms_more_icon" data-other="1">
                                                    <img src="/assets/images/svg/more.svg" alt=""/>
                                                </span>
                                            </div>
                                            <ul className="more_option">
                                                <li><a href="#"><span className="opt_icon"><span
                                                    className="icon icon_fav"></span></span>Add To Favourites</a></li>
                                                <li><a href="#"><span className="opt_icon"><span
                                                    className="icon icon_queue"></span></span>Add To Queue</a></li>
                                                <li><a href="#"><span className="opt_icon"><span
                                                    className="icon icon_dwn"></span></span>Download Now</a></li>
                                                <li><a href="#"><span className="opt_icon"><span
                                                    className="icon icon_playlst"></span></span>Add To Playlist</a></li>
                                                <li><a href="#"><span className="opt_icon"><span
                                                    className="icon icon_share"></span></span>Share</a></li>
                                            </ul>
                                        </div>
                                        <div className="ms_divider"></div>
                                        <div className="ms_weekly_box">
                                            <div className="weekly_left">
                                    <span className="w_top_no">
										02
									</span>
                                                <div className="w_top_song">
                                                    <div className="w_tp_song_img">
                                                        <img src="/assets/images/weekly/song2.jpg" alt=""
                                                             className="img-fluid"/>
                                                        <div className="ms_song_overlay">
                                                        </div>
                                                        <div className="ms_play_icon">
                                                            <img src="/assets/images/svg/play.svg" alt=""/>
                                                        </div>
                                                    </div>
                                                    <div className="w_tp_song_name">
                                                        <h3><a href="#">Walking Promises</a></h3>
                                                        <p>Ava Cornish</p>
                                                    </div>
                                                </div>
                                            </div>
                                            <div className="weekly_right">
                                                <span className="w_song_time">5:10</span>
                                                <span className="ms_more_icon" data-other="1">
                                                    <img src="/assets/images/svg/more.svg" alt=""/>
                                                </span>
                                            </div>
                                            <ul className="more_option">
                                                <li><a href="#"><span className="opt_icon"><span
                                                    className="icon icon_fav"></span></span>Add To Favourites</a></li>
                                                <li><a href="#"><span className="opt_icon"><span
                                                    className="icon icon_queue"></span></span>Add To Queue</a></li>
                                                <li><a href="#"><span className="opt_icon"><span
                                                    className="icon icon_dwn"></span></span>Download Now</a></li>
                                                <li><a href="#"><span className="opt_icon"><span
                                                    className="icon icon_playlst"></span></span>Add To Playlist</a></li>
                                                <li><a href="#"><span className="opt_icon"><span
                                                    className="icon icon_share"></span></span>Share</a></li>
                                            </ul>
                                        </div>
                                        <div className="ms_divider"></div>
                                        <div className="ms_weekly_box">
                                            <div className="weekly_left">
                                    <span className="w_top_no">
										03
									</span>
                                                <div className="w_top_song">
                                                    <div className="w_tp_song_img">
                                                        <img src="/assets/images/weekly/song3.jpg" alt=""
                                                             className="img-fluid"/>
                                                        <div className="ms_song_overlay">
                                                        </div>
                                                        <div className="ms_play_icon">
                                                            <img src="/assets/images/svg/play.svg" alt=""/>
                                                        </div>
                                                    </div>
                                                    <div className="w_tp_song_name">
                                                        <h3><a href="#">Gimme Some Courage</a></h3>
                                                        <p>Ava Cornish</p>
                                                    </div>
                                                </div>
                                            </div>
                                            <div className="weekly_right">
                                                <span className="w_song_time">5:10</span>
                                                <span className="ms_more_icon" data-other="1">
                                                    <img src="/assets/images/svg/more.svg" alt=""/>
                                                </span>
                                            </div>
                                            <ul className="more_option">
                                                <li><a href="#"><span className="opt_icon"><span
                                                    className="icon icon_fav"></span></span>Add To Favourites</a></li>
                                                <li><a href="#"><span className="opt_icon"><span
                                                    className="icon icon_queue"></span></span>Add To Queue</a></li>
                                                <li><a href="#"><span className="opt_icon"><span
                                                    className="icon icon_dwn"></span></span>Download Now</a></li>
                                                <li><a href="#"><span className="opt_icon"><span
                                                    className="icon icon_playlst"></span></span>Add To Playlist</a></li>
                                                <li><a href="#"><span className="opt_icon"><span
                                                    className="icon icon_share"></span></span>Share</a></li>
                                            </ul>
                                        </div>
                                        <div className="ms_divider"></div>
                                        <div className="ms_weekly_box">
                                            <div className="weekly_left">
                                    <span className="w_top_no">
										04
									</span>
                                                <div className="w_top_song">
                                                    <div className="w_tp_song_img">
                                                        <img src="/assets/images/weekly/song4.jpg" alt=""
                                                             className="img-fluid"/>
                                                        <div className="ms_song_overlay">
                                                        </div>
                                                        <div className="ms_play_icon">
                                                            <img src="/assets/images/svg/play.svg" alt=""/>
                                                        </div>
                                                    </div>
                                                    <div className="w_tp_song_name">
                                                        <h3><a href="#">Desired Games</a></h3>
                                                        <p>Ava Cornish</p>
                                                    </div>
                                                </div>
                                            </div>
                                            <div className="weekly_right">
                                                <span className="w_song_time">5:10</span>
                                                <span className="ms_more_icon" data-other="1">
                                                    <img src="/assets/images/svg/more.svg" alt=""/>
                                                </span>
                                            </div>
                                            <ul className="more_option">
                                                <li><a href="#"><span className="opt_icon"><span
                                                    className="icon icon_fav"></span></span>Add To Favourites</a></li>
                                                <li><a href="#"><span className="opt_icon"><span
                                                    className="icon icon_queue"></span></span>Add To Queue</a></li>
                                                <li><a href="#"><span className="opt_icon"><span
                                                    className="icon icon_dwn"></span></span>Download Now</a></li>
                                                <li><a href="#"><span className="opt_icon"><span
                                                    className="icon icon_playlst"></span></span>Add To Playlist</a></li>
                                                <li><a href="#"><span className="opt_icon"><span
                                                    className="icon icon_share"></span></span>Share</a></li>
                                            </ul>
                                        </div>
                                        <div className="ms_divider"></div>
                                        <div className="ms_weekly_box">
                                            <div className="weekly_left">
                                    <span className="w_top_no">
										05
									</span>
                                                <div className="w_top_song">
                                                    <div className="w_tp_song_img">
                                                        <img src="/assets/images/weekly/song5.jpg" alt=""
                                                             className="img-fluid"/>
                                                        <div className="ms_song_overlay">
                                                        </div>
                                                        <div className="ms_play_icon">
                                                            <img src="/assets/images/svg/play.svg" alt=""/>
                                                        </div>
                                                    </div>
                                                    <div className="w_tp_song_name">
                                                        <h3><a href="#">Dark Alley Acoustic</a></h3>
                                                        <p>Ava Cornish</p>
                                                    </div>
                                                </div>
                                            </div>
                                            <div className="weekly_right">
                                                <span className="w_song_time">5:10</span>
                                                <span className="ms_more_icon" data-other="1">
                                                    <img src="/assets/images/svg/more.svg" alt=""/>
                                                </span>
                                            </div>
                                            <ul className="more_option">
                                                <li><a href="#"><span className="opt_icon"><span
                                                    className="icon icon_fav"></span></span>Add To Favourites</a></li>
                                                <li><a href="#"><span className="opt_icon"><span
                                                    className="icon icon_queue"></span></span>Add To Queue</a></li>
                                                <li><a href="#"><span className="opt_icon"><span
                                                    className="icon icon_dwn"></span></span>Download Now</a></li>
                                                <li><a href="#"><span className="opt_icon"><span
                                                    className="icon icon_playlst"></span></span>Add To Playlist</a></li>
                                                <li><a href="#"><span className="opt_icon"><span
                                                    className="icon icon_share"></span></span>Share</a></li>
                                            </ul>
                                        </div>
                                    </div>
                                    <div className="col-lg-4 col-md-12 padding_right40">
                                        <div className="ms_weekly_box">
                                            <div className="weekly_left">
                                    <span className="w_top_no">
										06
									</span>
                                                <div className="w_top_song">
                                                    <div className="w_tp_song_img">
                                                        <img src="/assets/images/weekly/song6.jpg" alt=""
                                                             className="img-fluid"/>
                                                        <div className="ms_song_overlay">
                                                        </div>
                                                        <div className="ms_play_icon">
                                                            <img src="/assets/images/svg/play.svg" alt=""/>
                                                        </div>
                                                    </div>
                                                    <div className="w_tp_song_name">
                                                        <h3><a href="#">Walking Promises</a></h3>
                                                        <p>Ava Cornish</p>
                                                    </div>
                                                </div>
                                            </div>
                                            <div className="weekly_right">
                                                <span className="w_song_time">5:10</span>
                                                <span className="ms_more_icon" data-other="1">
										<img src="/assets/images/svg/more.svg" alt=""/>
									</span>
                                            </div>
                                            <ul className="more_option">
                                                <li><a href="#"><span className="opt_icon"><span
                                                    className="icon icon_fav"></span></span>Add To Favourites</a></li>
                                                <li><a href="#"><span className="opt_icon"><span
                                                    className="icon icon_queue"></span></span>Add To Queue</a></li>
                                                <li><a href="#"><span className="opt_icon"><span
                                                    className="icon icon_dwn"></span></span>Download Now</a></li>
                                                <li><a href="#"><span className="opt_icon"><span
                                                    className="icon icon_playlst"></span></span>Add To Playlist</a></li>
                                                <li><a href="#"><span className="opt_icon"><span
                                                    className="icon icon_share"></span></span>Share</a></li>
                                            </ul>
                                        </div>
                                        <div className="ms_divider"></div>
                                        <div className="ms_weekly_box">
                                            <div className="weekly_left">
                                    <span className="w_top_no">
										07
									</span>
                                                <div className="w_top_song">
                                                    <div className="w_tp_song_img">
                                                        <img src="/assets/images/weekly/song7.jpg" alt=""
                                                             className="img-fluid"/>
                                                        <div className="ms_song_overlay">
                                                        </div>
                                                        <div className="ms_play_icon">
                                                            <img src="/assets/images/svg/play.svg" alt=""/>
                                                        </div>
                                                    </div>
                                                    <div className="w_tp_song_name">
                                                        <h3><a href="#">Endless Things</a></h3>
                                                        <p>Ava Cornish</p>
                                                    </div>
                                                </div>
                                            </div>
                                            <div className="weekly_right">
                                                <span className="w_song_time">5:10</span>
                                                <span className="ms_more_icon" data-other="1">
										<img src="/assets/images/svg/more.svg" alt=""/>
									</span>
                                            </div>
                                            <ul className="more_option">
                                                <li><a href="#"><span className="opt_icon"><span
                                                    className="icon icon_fav"></span></span>Add To Favourites</a></li>
                                                <li><a href="#"><span className="opt_icon"><span
                                                    className="icon icon_queue"></span></span>Add To Queue</a></li>
                                                <li><a href="#"><span className="opt_icon"><span
                                                    className="icon icon_dwn"></span></span>Download Now</a></li>
                                                <li><a href="#"><span className="opt_icon"><span
                                                    className="icon icon_playlst"></span></span>Add To Playlist</a></li>
                                                <li><a href="#"><span className="opt_icon"><span
                                                    className="icon icon_share"></span></span>Share</a></li>
                                            </ul>
                                        </div>
                                        <div className="ms_divider"></div>
                                        <div className="ms_weekly_box ms_active_play">
                                            <div className="weekly_left">
                                    <span className="w_top_no">
										08
									</span>
                                                <div className="w_top_song">
                                                    <div className="w_tp_song_img">
                                                        <img src="/assets/images/weekly/song8.jpg" alt=""
                                                             className="img-fluid"/>
                                                        <div className="ms_song_overlay">
                                                        </div>
                                                        <div className="ms_play_icon">
                                                            <div className="ms_bars">
                                                                <div className="bar"></div>
                                                                <div className="bar"></div>
                                                                <div className="bar"></div>
                                                                <div className="bar"></div>
                                                                <div className="bar"></div>
                                                                <div className="bar"></div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div className="w_tp_song_name">
                                                        <h3><a href="#">Dream Your Moments</a></h3>
                                                        <p>Ava Cornish</p>
                                                    </div>
                                                </div>
                                            </div>
                                            <div className="weekly_right">
                                                <span className="w_song_time">5:10</span>
                                                <span className="ms_more_icon" data-other="1">
										<img src="/assets/images/svg/more.svg" alt=""/>
									</span>
                                            </div>
                                            <ul className="more_option">
                                                <li><a href="#"><span className="opt_icon"><span
                                                    className="icon icon_fav"></span></span>Add To Favourites</a></li>
                                                <li><a href="#"><span className="opt_icon"><span
                                                    className="icon icon_queue"></span></span>Add To Queue</a></li>
                                                <li><a href="#"><span className="opt_icon"><span
                                                    className="icon icon_dwn"></span></span>Download Now</a></li>
                                                <li><a href="#"><span className="opt_icon"><span
                                                    className="icon icon_playlst"></span></span>Add To Playlist</a></li>
                                                <li><a href="#"><span className="opt_icon"><span
                                                    className="icon icon_share"></span></span>Share</a></li>
                                            </ul>
                                        </div>
                                        <div className="ms_divider"></div>
                                        <div className="ms_weekly_box">
                                            <div className="weekly_left">
                                    <span className="w_top_no">
										09
									</span>
                                                <div className="w_top_song">
                                                    <div className="w_tp_song_img">
                                                        <img src="/assets/images/weekly/song9.jpg" alt=""
                                                             className="img-fluid"/>
                                                        <div className="ms_song_overlay">
                                                        </div>
                                                        <div className="ms_play_icon">
                                                            <img src="/assets/images/svg/play.svg" alt=""/>
                                                        </div>
                                                    </div>
                                                    <div className="w_tp_song_name">
                                                        <h3><a href="#">Until I Met You</a></h3>
                                                        <p>Ava Cornish</p>
                                                    </div>
                                                </div>
                                            </div>
                                            <div className="weekly_right">
                                                <span className="w_song_time">5:10</span>
                                                <span className="ms_more_icon" data-other="1">
										<img src="/assets/images/svg/more.svg" alt=""/>
									</span>
                                            </div>
                                            <ul className="more_option">
                                                <li><a href="#"><span className="opt_icon"><span
                                                    className="icon icon_fav"></span></span>Add To Favourites</a></li>
                                                <li><a href="#"><span className="opt_icon"><span
                                                    className="icon icon_queue"></span></span>Add To Queue</a></li>
                                                <li><a href="#"><span className="opt_icon"><span
                                                    className="icon icon_dwn"></span></span>Download Now</a></li>
                                                <li><a href="#"><span className="opt_icon"><span
                                                    className="icon icon_playlst"></span></span>Add To Playlist</a></li>
                                                <li><a href="#"><span className="opt_icon"><span
                                                    className="icon icon_share"></span></span>Share</a></li>
                                            </ul>
                                        </div>
                                        <div className="ms_divider"></div>
                                        <div className="ms_weekly_box">
                                            <div className="weekly_left">
                                    <span className="w_top_no">
										10
									</span>
                                                <div className="w_top_song">
                                                    <div className="w_tp_song_img">
                                                        <img src="/assets/images/weekly/song5.jpg" alt=""
                                                             className="img-fluid"/>
                                                        <div className="ms_song_overlay">
                                                        </div>
                                                        <div className="ms_play_icon">
                                                            <img src="/assets/images/svg/play.svg" alt=""/>
                                                        </div>
                                                    </div>
                                                    <div className="w_tp_song_name">
                                                        <h3><a href="#">Gimme Some Courage</a></h3>
                                                        <p>Ava Cornish</p>
                                                    </div>
                                                </div>
                                            </div>
                                            <div className="weekly_right">
                                                <span className="w_song_time">5:10</span>
                                                <span className="ms_more_icon" data-other="1">
										<img src="/assets/images/svg/more.svg" alt=""/>
									</span>
                                            </div>
                                            <ul className="more_option">
                                                <li><a href="#"><span className="opt_icon"><span
                                                    className="icon icon_fav"></span></span>Add To Favourites</a></li>
                                                <li><a href="#"><span className="opt_icon"><span
                                                    className="icon icon_queue"></span></span>Add To Queue</a></li>
                                                <li><a href="#"><span className="opt_icon"><span
                                                    className="icon icon_dwn"></span></span>Download Now</a></li>
                                                <li><a href="#"><span className="opt_icon"><span
                                                    className="icon icon_playlst"></span></span>Add To Playlist</a></li>
                                                <li><a href="#"><span className="opt_icon"><span
                                                    className="icon icon_share"></span></span>Share</a></li>
                                            </ul>
                                        </div>
                                    </div>
                                    <div className="col-lg-4 col-md-12">
                                        <div className="ms_weekly_box">
                                            <div className="weekly_left">
                                    <span className="w_top_no">
										11
									</span>
                                                <div className="w_top_song">
                                                    <div className="w_tp_song_img">
                                                        <img src="/assets/images/weekly/song2.jpg" alt=""
                                                             className="img-fluid"/>
                                                        <div className="ms_song_overlay">
                                                        </div>
                                                        <div className="ms_play_icon">
                                                            <img src="/assets/images/svg/play.svg" alt=""/>
                                                        </div>
                                                    </div>
                                                    <div className="w_tp_song_name">
                                                        <h3><a href="#">Dark Alley Acoustic</a></h3>
                                                        <p>Ava Cornish</p>
                                                    </div>
                                                </div>
                                            </div>
                                            <div className="weekly_right">
                                                <span className="w_song_time">5:10</span>
                                                <span className="ms_more_icon" data-other="1">
										<img src="/assets/images/svg/more.svg" alt=""/>
									</span>
                                            </div>
                                            <ul className="more_option">
                                                <li><a href="#"><span className="opt_icon"><span
                                                    className="icon icon_fav"></span></span>Add To Favourites</a></li>
                                                <li><a href="#"><span className="opt_icon"><span
                                                    className="icon icon_queue"></span></span>Add To Queue</a></li>
                                                <li><a href="#"><span className="opt_icon"><span
                                                    className="icon icon_dwn"></span></span>Download Now</a></li>
                                                <li><a href="#"><span className="opt_icon"><span
                                                    className="icon icon_playlst"></span></span>Add To Playlist</a></li>
                                                <li><a href="#"><span className="opt_icon"><span
                                                    className="icon icon_share"></span></span>Share</a></li>
                                            </ul>
                                        </div>
                                        <div className="ms_divider"></div>
                                        <div className="ms_weekly_box">
                                            <div className="weekly_left">
                                    <span className="w_top_no">
										12
									</span>
                                                <div className="w_top_song">
                                                    <div className="w_tp_song_img">
                                                        <img src="/assets/images/weekly/song11.jpg" alt=""
                                                             className="img-fluid"/>
                                                        <div className="ms_song_overlay">
                                                        </div>
                                                        <div className="ms_play_icon">
                                                            <img src="/assets/images/svg/play.svg" alt=""/>
                                                        </div>
                                                    </div>
                                                    <div className="w_tp_song_name">
                                                        <h3><a href="#">The Heartbeat Stops</a></h3>
                                                        <p>Ava Cornish</p>
                                                    </div>
                                                </div>
                                            </div>
                                            <div className="weekly_right">
                                                <span className="w_song_time">5:10</span>
                                                <span className="ms_more_icon" data-other="1">
										<img src="/assets/images/svg/more.svg" alt=""/>
									</span>
                                            </div>
                                            <ul className="more_option">
                                                <li><a href="#"><span className="opt_icon"><span
                                                    className="icon icon_fav"></span></span>Add To Favourites</a></li>
                                                <li><a href="#"><span className="opt_icon"><span
                                                    className="icon icon_queue"></span></span>Add To Queue</a></li>
                                                <li><a href="#"><span className="opt_icon"><span
                                                    className="icon icon_dwn"></span></span>Download Now</a></li>
                                                <li><a href="#"><span className="opt_icon"><span
                                                    className="icon icon_playlst"></span></span>Add To Playlist</a></li>
                                                <li><a href="#"><span className="opt_icon"><span
                                                    className="icon icon_share"></span></span>Share</a></li>
                                            </ul>
                                        </div>
                                        <div className="ms_divider"></div>
                                        <div className="ms_weekly_box">
                                            <div className="weekly_left">
                                    <span className="w_top_no">
										13
									</span>
                                                <div className="w_top_song">
                                                    <div className="w_tp_song_img">
                                                        <img src="/assets/images/weekly/song12.jpg" alt=""
                                                             className="img-fluid"/>
                                                        <div className="ms_song_overlay">
                                                        </div>
                                                        <div className="ms_play_icon">
                                                            <img src="/assets/images/svg/play.svg" alt=""/>
                                                        </div>
                                                    </div>
                                                    <div className="w_tp_song_name">
                                                        <h3><a href="#">One More Stranger</a></h3>
                                                        <p>Ava Cornish</p>
                                                    </div>
                                                </div>
                                            </div>
                                            <div className="weekly_right">
                                                <span className="w_song_time">5:10</span>
                                                <span className="ms_more_icon" data-other="1">
										<img src="/assets/images/svg/more.svg" alt=""/>
									</span>
                                            </div>
                                            <ul className="more_option">
                                                <li><a href="#"><span className="opt_icon"><span
                                                    className="icon icon_fav"></span></span>Add To Favourites</a></li>
                                                <li><a href="#"><span className="opt_icon"><span
                                                    className="icon icon_queue"></span></span>Add To Queue</a></li>
                                                <li><a href="#"><span className="opt_icon"><span
                                                    className="icon icon_dwn"></span></span>Download Now</a></li>
                                                <li><a href="#"><span className="opt_icon"><span
                                                    className="icon icon_playlst"></span></span>Add To Playlist</a></li>
                                                <li><a href="#"><span className="opt_icon"><span
                                                    className="icon icon_share"></span></span>Share</a></li>
                                            </ul>
                                        </div>
                                        <div className="ms_divider"></div>
                                        <div className="ms_weekly_box">
                                            <div className="weekly_left">
                                    <span className="w_top_no">
										14
									</span>
                                                <div className="w_top_song">
                                                    <div className="w_tp_song_img">
                                                        <img src="/assets/images/weekly/song13.jpg" alt=""
                                                             className="img-fluid"/>
                                                        <div className="ms_song_overlay">
                                                        </div>
                                                        <div className="ms_play_icon">
                                                            <img src="/assets/images/svg/play.svg" alt=""/>
                                                        </div>
                                                    </div>
                                                    <div className="w_tp_song_name">
                                                        <h3><a href="#">Walking Promises</a></h3>
                                                        <p>Ava Cornish</p>
                                                    </div>
                                                </div>
                                            </div>
                                            <div className="weekly_right">
                                                <span className="w_song_time">5:10</span>
                                                <span className="ms_more_icon" data-other="1">
										<img src="/assets/images/svg/more.svg" alt=""/>
									</span>
                                            </div>
                                            <ul className="more_option">
                                                <li><a href="#"><span className="opt_icon"><span
                                                    className="icon icon_fav"></span></span>Add To Favourites</a></li>
                                                <li><a href="#"><span className="opt_icon"><span
                                                    className="icon icon_queue"></span></span>Add To Queue</a></li>
                                                <li><a href="#"><span className="opt_icon"><span
                                                    className="icon icon_dwn"></span></span>Download Now</a></li>
                                                <li><a href="#"><span className="opt_icon"><span
                                                    className="icon icon_playlst"></span></span>Add To Playlist</a></li>
                                                <li><a href="#"><span className="opt_icon"><span
                                                    className="icon icon_share"></span></span>Share</a></li>
                                            </ul>
                                        </div>
                                        <div className="ms_divider"></div>
                                        <div className="ms_weekly_box">
                                            <div className="weekly_left">
                                    <span className="w_top_no">
										15
									</span>
                                                <div className="w_top_song">
                                                    <div className="w_tp_song_img">
                                                        <img src="/assets/images/weekly/song14.jpg" alt=""
                                                             className="img-fluid"/>
                                                        <div className="ms_song_overlay">
                                                        </div>
                                                        <div className="ms_play_icon">
                                                            <img src="/assets/images/svg/play.svg" alt=""/>
                                                        </div>
                                                    </div>
                                                    <div className="w_tp_song_name">
                                                        <h3><a href="#">Endless Things</a></h3>
                                                        <p>Ava Cornish</p>
                                                    </div>
                                                </div>
                                            </div>
                                            <div className="weekly_right">
                                                <span className="w_song_time">5:10</span>
                                                <span className="ms_more_icon" data-other="1">
                                                    <img src="/assets/images/svg/more.svg" alt=""/>
                                                </span>
                                            </div>
                                            <ul className="more_option">
                                                <li><a href="#"><span className="opt_icon"><span
                                                    className="icon icon_fav"></span></span>Add To Favourites</a></li>
                                                <li><a href="#"><span className="opt_icon"><span
                                                    className="icon icon_queue"></span></span>Add To Queue</a></li>
                                                <li><a href="#"><span className="opt_icon"><span
                                                    className="icon icon_dwn"></span></span>Download Now</a></li>
                                                <li><a href="#"><span className="opt_icon"><span
                                                    className="icon icon_playlst"></span></span>Add To Playlist</a></li>
                                                <li><a href="#"><span className="opt_icon"><span
                                                    className="icon icon_share"></span></span>Share</a></li>
                                            </ul>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        {/**Add Section Start**/}
                        <div className="ms_advr_wrapper">
                            <div className="container">
                                <div className="row">
                                    <div className="col-lg-12">
                                        <a href="#"><img src="/assets/images/adv.jpg" alt="" className="img-fluid"/></a>
                                    </div>
                                </div>
                            </div>
                        </div>


                        {/**Featured Albumn Section Start**/}
                        <AlbumInteresse {...this.props} />

                        {/**Top Genres Section Start**/}
                        <GenreInteresse {...this.props} />


                       { /**Add Section Start**/}
                        <div className="ms_advr_wrapper ms_advr2">
                            <div className="container">
                                <div className="row">
                                    <div className="col-lg-12">
                                        <a href="#"><img src="/assets/images/album2.jpg" alt=""
                                                         className="img-fluid"/></a>
                                    </div>
                                </div>
                            </div>
                        </div>

                       { /**Live Radio Section Start**/}
                        <StationradioInteresse {...this.props} />

                    </div>

                   <FooterMini/>

                   { /***Audio Player Section**/}



                </div>

                </div>

                <FooterMultiple/>

            </>
        )
    }
}
export default DasborbIndexSite;

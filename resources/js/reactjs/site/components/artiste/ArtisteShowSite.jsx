import React, {Component} from 'react';
import HelmetSite from "../inc/vendor/HelmetSite";
import VerticalNaveSite from "../inc/VerticalNaveSite";
import HorizontaNaveSite from "../inc/HorizontaNaveSite";
import {Link} from "react-router-dom";
import FooterMini from "../inc/FooterMini";
import FooterMultiple from "../inc/FooterMultiple";
import UndoRedo from "../inc/vendor/UndoRedo";
import AlbumUserInteresse from "../albums/AlbumUserInteresse";
import PropTypes from "prop-types";
import {getArtisteReselect} from "../../redux/selector";
import {connect} from "react-redux";
import {favoriteItem, loadShowdata, unfavoriteItem} from "../../redux/actions/artisteAction";
import ReadMoreAndLess from "react-read-more-less";
import ButtonFavorite from "../inc/vendor/ButtonFavorite";
const abbrev = ['', 'k', 'M', 'B', 'T'];


class ArtisteShowSite extends Component {

    componentDidMount() {
        this.props.loadShowdata(this.props);
    }

    data_countfavoritesFormatter(countfavorites, precision) {
        const unrangifiedOrder = Math.floor(Math.log10(Math.abs(countfavorites)) / 3);
        const order = Math.max(0, Math.min(unrangifiedOrder, abbrev.length -1 ));
        const suffix = abbrev[order];
        return (countfavorites / Math.pow(10, order * 3)).toFixed(precision) + suffix;
    }
    render() {
        const {user} = this.props;
        return (
            <>
                <HelmetSite title={`${user.name || $name_site} | ${$name_site}  `}/>

                <div className="ms_main_wrapper">


                    <HorizontaNaveSite/>

                    <VerticalNaveSite/>

                    <div className="ms_album_single_wrapper ms_artist_single">

                        <UndoRedo {...this.props}/>

                        <div className="album_single_data">
                            <div className="album_single_img">
                                <img src={user.avatar} alt={user.name} className="img-fluid"/>
                            </div>
                            {user.slug && (
                                <div className="album_single_text">
                                    <h2>{user.name}</h2>
                                    <p className="singer_name">{this.data_countfavoritesFormatter(user.countfavorites || "")} {user.countfavorites > 1 ? "abonnés" : "abonné"}</p>
                                    <p className="singer_name">{user.city_name && (<>{user.name} -</>)} {user.country.name}</p>
                                    <div className="about_artist">
                                        <ReadMoreAndLess
                                            className="read-more-content"
                                            charLimit={100}
                                            readMoreText="+"
                                            readLessText="--"
                                        >
                                            {user.description || ""}
                                        </ReadMoreAndLess>
                                    </div>
                                    <div className="album_btn">
                                        {/*
                                     <a href="#" className="ms_btn play_btn">
                                        <span className="play_all">
                                            <img src="/assets/images/svg/play_all.svg" alt=""/>Play</span>
                                    </a>
                                    */}

                                        <a href="#" className="ms_btn play_btn">
                                           <span className="play_all">
                                            <img src="/assets/images/svg/pause_all.svg" alt=""/>Pause</span>
                                        </a>

                                        <ButtonFavorite  {...this.props} {...user}
                                                         unfavoriteItem={this.props.unfavoriteItem}
                                                         favoriteItem={this.props.favoriteItem} />
                                    </div>
                                </div>
                            )}
                        </div>

                        <div className="album_inner_list">
                            <div className="album_list_wrapper">
                                <ul className="album_list_name">
                                    <li>#</li>
                                    <li>Title</li>
                                    <li className="text-center"></li>
                                    <li className="text-center"></li>
                                    <li className="text-center"></li>
                                    <li className="text-center"></li>
                                </ul>
                                <ul className="play_active_song">
                                    <li><a href="#"><span className="play_no">1</span><span className="play_hover"></span></a></li>
                                    <li><a href="#">Dark Alley Acoustic</a></li>
                                    <li className="text-center"><a href="#">5:26</a></li>
                                    <li className="text-center"><a href="#"><span
                                        className="ms_icon1 ms_fav_icon"></span></a></li>
                                    <li className="text-center">1 222 3873</li>
                                    <li className="text-center ms_more_icon"><a href="#"><span className="ms_icon1 ms_active_icon"></span></a>
                                        <ul className="more_option">
                                            <li><a href="#"><span className="opt_icon"><span className="icon icon_fav"></span></span>Add To Favourites</a></li>
                                            <li><a href="#"><span className="opt_icon"><span className="icon icon_queue"></span></span>Add To Queue</a></li>
                                            <li><a href="#"><span className="opt_icon"><span className="icon icon_dwn"></span></span>Download Now</a></li>
                                            <li><a href="#"><span className="opt_icon"><span className="icon icon_playlst"></span></span>Add To Playlist</a></li>
                                            <li><a href="#"><span className="opt_icon"><span className="icon icon_share"></span></span>Share</a></li>
                                        </ul>
                                    </li>
                                </ul>
                                <ul>
                                    <li><a href="#"><span className="play_no">2</span><span className="play_hover"></span></a></li>
                                    <li><a href="#">Cloud nine</a></li>
                                    <li className="text-center"><a href="#">5:26</a></li>
                                    <li className="text-center"><a href="#"><span
                                        className="ms_icon1 ms_fav_icon"></span></a></li>
                                    <li className="text-center">546 383</li>
                                    <li className="text-center ms_more_icon"><a href="#"><span className="ms_icon1 ms_active_icon"></span></a>
                                        <ul className="more_option">
                                            <li><a href="#"><span className="opt_icon"><span className="icon icon_fav"></span></span>Add To Favourites</a></li>
                                            <li><a href="#"><span className="opt_icon"><span className="icon icon_queue"></span></span>Add To Queue</a></li>
                                            <li><a href="#"><span className="opt_icon"><span className="icon icon_dwn"></span></span>Download Now</a></li>
                                            <li><a href="#"><span className="opt_icon"><span className="icon icon_playlst"></span></span>Add To Playlist</a></li>
                                            <li><a href="#"><span className="opt_icon"><span className="icon icon_share"></span></span>Share</a></li>
                                        </ul>
                                    </li>
                                </ul>
                                <ul>
                                    <li><a href="#"><span className="play_no">3</span><span className="play_hover"></span></a></li>
                                    <li><a href="#">Cloud nine</a></li>
                                    <li className="text-center"><a href="#">5:26</a></li>
                                    <li className="text-center"><a href="#"><span
                                        className="ms_icon1 ms_fav_icon"></span></a></li>
                                    <li className="text-center">122 373</li>
                                    <li className="text-center ms_more_icon"><a href="#"><span className="ms_icon1 ms_active_icon"></span></a>
                                        <ul className="more_option">
                                            <li><a href="#"><span className="opt_icon"><span className="icon icon_fav"></span></span>Add To Favourites</a></li>
                                            <li><a href="#"><span className="opt_icon"><span className="icon icon_queue"></span></span>Add To Queue</a></li>
                                            <li><a href="#"><span className="opt_icon"><span className="icon icon_dwn"></span></span>Download Now</a></li>
                                            <li><a href="#"><span className="opt_icon"><span className="icon icon_playlst"></span></span>Add To Playlist</a></li>
                                            <li><a href="#"><span className="opt_icon"><span className="icon icon_share"></span></span>Share</a></li>
                                        </ul>
                                    </li>
                                </ul>
                                <ul>
                                    <li><a href="#"><span className="play_no">4</span><span className="play_hover"></span></a></li>
                                    <li><a href="#">Cloud nine</a></li>
                                    <li className="text-center"><a href="#">5:26</a></li>
                                    <li className="text-center"><a href="#"><span
                                        className="ms_icon1 ms_fav_icon"></span></a></li>
                                    <li className="text-center">278 373</li>
                                    <li className="text-center ms_more_icon"><a href="#"><span className="ms_icon1 ms_active_icon"></span></a>
                                        <ul className="more_option">
                                            <li><a href="#"><span className="opt_icon"><span className="icon icon_fav"></span></span>Add To Favourites</a></li>
                                            <li><a href="#"><span className="opt_icon"><span className="icon icon_queue"></span></span>Add To Queue</a></li>
                                            <li><a href="#"><span className="opt_icon"><span className="icon icon_dwn"></span></span>Download Now</a></li>
                                            <li><a href="#"><span className="opt_icon"><span className="icon icon_playlst"></span></span>Add To Playlist</a></li>
                                            <li><a href="#"><span className="opt_icon"><span className="icon icon_share"></span></span>Share</a></li>
                                        </ul>
                                    </li>
                                </ul>
                                <ul>
                                    <li><a href="#"><span className="play_no">5</span><span className="play_hover"></span></a></li>
                                    <li><a href="#">Cloud nine</a></li>
                                    <li className="text-center"><a href="#">5:26</a></li>
                                    <li className="text-center"><a href="#"><span
                                        className="ms_icon1 ms_fav_icon"></span></a></li>
                                    <li className="text-center">8 373</li>
                                    <li className="text-center ms_more_icon"><a href="#"><span className="ms_icon1 ms_active_icon"></span></a>
                                        <ul className="more_option">
                                            <li><a href="#"><span className="opt_icon"><span className="icon icon_fav"></span></span>Add To Favourites</a></li>
                                            <li><a href="#"><span className="opt_icon"><span className="icon icon_queue"></span></span>Add To Queue</a></li>
                                            <li><a href="#"><span className="opt_icon"><span className="icon icon_dwn"></span></span>Download Now</a></li>
                                            <li><a href="#"><span className="opt_icon"><span className="icon icon_playlst"></span></span>Add To Playlist</a></li>
                                            <li><a href="#"><span className="opt_icon"><span className="icon icon_share"></span></span>Share</a></li>
                                        </ul>
                                    </li>
                                </ul>
                                <ul>
                                    <li><a href="#"><span className="play_no">6</span><span className="play_hover"></span></a></li>
                                    <li><a href="#">Cloud nine</a></li>
                                    <li className="text-center"><a href="#">5:26</a></li>
                                    <li className="text-center"><a href="#"><span
                                        className="ms_icon1 ms_fav_icon"></span></a></li>
                                    <li className="text-center">98 373</li>
                                    <li className="text-center ms_more_icon"><a href="#"><span className="ms_icon1 ms_active_icon"></span></a>
                                        <ul className="more_option">
                                            <li><a href="#"><span className="opt_icon"><span className="icon icon_fav"></span></span>Add To Favourites</a></li>
                                            <li><a href="#"><span className="opt_icon"><span className="icon icon_queue"></span></span>Add To Queue</a></li>
                                            <li><a href="#"><span className="opt_icon"><span className="icon icon_dwn"></span></span>Download Now</a></li>
                                            <li><a href="#"><span className="opt_icon"><span className="icon icon_playlst"></span></span>Add To Playlist</a></li>
                                            <li><a href="#"><span className="opt_icon"><span className="icon icon_share"></span></span>Share</a></li>
                                        </ul>
                                    </li>
                                </ul>
                            </div>
                        </div>


                        <div className="ms_fea_album_slider">
                            <div className="ms_heading">
                                <h1>Projet de  {user.name}</h1>
                            </div>

                            <AlbumUserInteresse {...this.props} />

                        </div>

                    </div>


                    <FooterMini/>

                    { /***Audio Player Section**/}


                </div>

                <FooterMultiple/>

            </>
        );
    }
}
ArtisteShowSite.propTypes = {
    loadShowdata: PropTypes.func.isRequired,
};

const mapStoreToProps = store => ({
    user: getArtisteReselect(store),
});

export default connect(mapStoreToProps, {
    loadShowdata,unfavoriteItem,favoriteItem,
})(ArtisteShowSite);

import React, {Component} from 'react';
import HelmetSite from "../inc/vendor/HelmetSite";
import VerticalNaveSite from "../inc/VerticalNaveSite";
import HorizontaNaveSite from "../inc/HorizontaNaveSite";
import {Link} from "react-router-dom";
import FooterMini from "../inc/FooterMini";
import FooterMultiple from "../inc/FooterMultiple";
import UndoRedo from "../inc/vendor/UndoRedo";

class ArtisteIndexSite extends Component {
    render() {
        return (
            <>
                <HelmetSite title={`Artistes `}/>

                <div className="ms_main_wrapper">

                    <VerticalNaveSite/>

                    <div className="ms_content_wrapper padder_top80">

                        <HorizontaNaveSite/>


                        { /**Banner**/}
                        <div className="ms-banner">

                            {/**Featured Albumn Section Start**/}
                            <div className="ms_top_artist">
                                <div className="container-fluid">

                                    <UndoRedo {...this.props}/>

                                    <div className="row">
                                        <div className="col-lg-12">
                                            <div className="ms_heading">
                                                <h1> Artists</h1>
                                            </div>
                                        </div>
                                        <div className="col-sm-2 mx-auto">
                                            <div className="ms_rcnt_box marger_bottom30">
                                                <div className="ms_rcnt_box_img">
                                                   <Link to={`/artiste/show/`}>
                                                       <img src="/assets/images/featured/song1.jpg"
                                                            alt="" className="img-fluid"/>
                                                   </Link>
                                                        <div className="ms_main_overlay">
                                                            <div className="ms_play_icon">
                                                                <img src="/assets/images/svg/play.svg" alt=""/>
                                                            </div>
                                                        </div>
                                                </div>
                                                <div className="ms_rcnt_box_text">
                                                    <h3><Link to={`/artiste/show/`}>Boclair Temgoua</Link></h3>
                                                </div>
                                            </div>
                                        </div>
                                        <div className="col-sm-2 mx-auto">
                                            <div className="ms_rcnt_box marger_bottom30">
                                                <div className="ms_rcnt_box_img">
                                                    <img src="/assets/images/artist/artist13.jpg" alt="" className="img-fluid"/>
                                                        <div className="ms_main_overlay">
                                                            <div className="ms_play_icon">
                                                                <img src="/assets/images/svg/play.svg" alt=""/>
                                                            </div>
                                                        </div>
                                                </div>
                                                <div className="ms_rcnt_box_text">
                                                    <h3><a href="artist_single.html">Leah Knox</a></h3>
                                                </div>
                                            </div>
                                        </div>
                                        <div className="col-sm-2 mx-auto">
                                            <div className="ms_rcnt_box marger_bottom30">
                                                <div className="ms_rcnt_box_img">
                                                    <img src="/assets/images/featured/song4.jpg" alt="" className="img-fluid"/>
                                                        <div className="ms_main_overlay">
                                                            <div className="ms_play_icon">
                                                                <img src="/assets/images/svg/play.svg" alt=""/>
                                                            </div>
                                                        </div>
                                                </div>
                                                <div className="ms_rcnt_box_text">
                                                    <h3><a href="artist_single.html">Charles Davidson</a></h3>
                                                </div>
                                            </div>
                                        </div>
                                        <div className="col-sm-2 mx-auto">
                                            <div className="ms_rcnt_box">
                                                <div className="ms_rcnt_box_img">
                                                    <img src="/assets/images/album/album2.jpg" alt="" className="img-fluid"/>
                                                        <div className="ms_main_overlay">
                                                            <div className="ms_play_icon">
                                                                <img src="/assets/images/svg/play.svg" alt=""/>
                                                            </div>
                                                        </div>
                                                </div>
                                                <div className="ms_rcnt_box_text">
                                                    <h3><a href="artist_single.html">Vanessa Hunter</a></h3>
                                                </div>
                                            </div>
                                        </div>
                                        <div className="col-sm-2 mx-auto">
                                            <div className="ms_rcnt_box">
                                                <div className="ms_rcnt_box_img">
                                                    <img src="/assets/images/album/album3.jpg" alt="" className="img-fluid"/>
                                                        <div className="ms_main_overlay">
                                                            <div className="ms_play_icon">
                                                                <img src="/assets/images/svg/play.svg" alt=""/>
                                                            </div>
                                                        </div>
                                                </div>
                                                <div className="ms_rcnt_box_text">
                                                    <h3><a href="artist_single.html">Sophie Hudson</a></h3>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            {/**Add Section Start**/}
                            <div className="ms_advr_wrapper">
                                <div className="container">
                                    <div className="row">
                                        <div className="col-lg-12">
                                            <a href="#"><img src="/assets/images/adv.jpg" alt="" className="img-fluid"/></a>
                                        </div>
                                    </div>
                                </div>
                            </div>


                        </div>

                        <FooterMini/>

                        { /***Audio Player Section**/}



                    </div>

                </div>

                <FooterMultiple/>

            </>
        );
    }
}

export default ArtisteIndexSite;

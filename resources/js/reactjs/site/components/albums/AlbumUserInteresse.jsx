import React, {PureComponent} from 'react';
import PropTypes from "prop-types";
import {connect} from "react-redux";
import {
    loadAlldatainteruser,
} from "../../redux/actions/albumAction";
import AlbumUserList from "./inc/AlbumUserList";

class AlbumUserInteresse extends PureComponent {

    componentDidMount() {
        this.props.loadAlldatainteruser(this.props);
    }

    render() {
        const {musics} = this.props;
        const mapMusics = musics.length >= 0 ? (
            musics.map(item => {
                return (
                    <AlbumUserList key={item.id} {...item}  />
                )
            })
        ) : (
            <></>
        );
        return (
            <>
                <div className="row">

                    {mapMusics}

                </div>
            </>
        );
    }
}

AlbumUserInteresse.propTypes = {
    loadAlldatainteruser: PropTypes.func.isRequired,
};

const mapStoreToProps = (store) => ({
    musics: store.albums.items,
});

export default connect(mapStoreToProps, {
    loadAlldatainteruser,
})(AlbumUserInteresse);

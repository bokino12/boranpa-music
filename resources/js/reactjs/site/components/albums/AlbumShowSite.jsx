import React, {Component} from 'react';
import HelmetSite from "../inc/vendor/HelmetSite";
import VerticalNaveSite from "../inc/VerticalNaveSite";
import HorizontaNaveSite from "../inc/HorizontaNaveSite";
import {Link} from "react-router-dom";
import FooterMultiple from "../inc/FooterMultiple";
import FooterMini from "../inc/FooterMini";
import UndoRedo from "../inc/vendor/UndoRedo";
import PropTypes from "prop-types";
import {getAlbumReselect} from "../../redux/selector";
import {connect} from "react-redux";
import {loadShowdata,unfavoriteItem,favoriteItem} from "../../redux/actions/albumAction";
import PlayAllIcon from '../../assets/images/svg/play_all.svg';
import PauseAllIcon from '../../assets/images/svg/pause_all.svg';
import AlbumUserInteresse from "./AlbumUserInteresse";
import ButtonFavorite from "../inc/vendor/ButtonFavorite";


class AlbumShowSite extends Component {
    constructor(props) {
        super(props);
        this.state = {
            //
        }
    }

    componentDidMount() {

        this.props.loadShowdata(this.props);
    }

    render() {
        const {music} =this.props;
        return (
            <>
                <HelmetSite title={`${music.name + " - "+ music.user.name || $name_site} | ${$name_site}  `}/>

                <div className="ms_main_wrapper">



                    <HorizontaNaveSite/>
                    <VerticalNaveSite/>

                    <div className="ms_album_single_wrapper">

                        <UndoRedo {...this.props}/>

                        <div className="album_single_data">
                            <div className="album_single_img">
                                <img src={music.photo} alt={music.name} className="img-fluid"/>
                            </div>
                            {music.slugin && (
                                <div className="album_single_text">
                                    <h2>{music.name}</h2>
                                    <Link to={`/user/${music.user.slug}/`}><p className="singer_name">{music.user.name}</p></Link>
                                    <div className="album_feature">
                                        <p className="singer_name">{music.category === 2 ? "Album" :"Single"} | {music.tracks_count} {music.tracks_count > 2 ? "songs" :"song"} | 25:10</p>
                                        <p className="singer_name">{music.year_production} | {music.agence_production}</p>
                                    </div>
                                    <div className="album_btn">
                                        <a href="#" className="ms_btn play_btn">
                                        <span className="play_all">
                                            <img src={PlayAllIcon} alt=""/>Play All</span>
                                        </a>
                                        {/*

                                    <a href="#" className="ms_btn play_btn">
                                           <span className="play_all">
                                            <img src={PauseAllIcon} alt=""/>Pause</span>
                                    </a>
                                    */}
                                        <ButtonFavorite  {...this.props} {...music}
                                                         unfavoriteItem={this.props.unfavoriteItem}
                                                         favoriteItem={this.props.favoriteItem} />
                                    </div>
                                </div>
                            )}
                        </div>

                        <div className="album_inner_list">
                            <div className="album_list_wrapper">
                                <ul className="album_list_name">
                                    <li>#</li>
                                    <li>Title</li>
                                    <li className="text-center"></li>
                                    <li className="text-center"></li>
                                    <li className="text-center"></li>
                                    <li className="text-center"></li>
                                </ul>
                                <ul className="play_active_song">
                                    <li><Link to={`/artiste/show/`}><span className="play_no">1</span><span className="play_hover"></span></Link></li>
                                    <li><a href="#">Dark Alley Acoustic</a></li>
                                    <li className="text-center"><a href="#">5:26</a></li>
                                    <li className="text-center"><a href="#"><span
                                        className="ms_icon1 ms_fav_icon"></span></a></li>
                                    <li className="text-center">278 373</li>
                                    <li className="text-center ms_more_icon">
                                        <a href="#"><span className="ms_icon1 ms_active_icon"></span></a>
                                        <ul className="more_option">
                                            <li><a href="#"><span className="opt_icon"><span className="icon icon_fav"></span></span>Add To Favourites</a></li>
                                            <li><a href="#"><span className="opt_icon"><span className="icon icon_queue"></span></span>Add To Queue</a></li>
                                            <li><a href="#"><span className="opt_icon"><span className="icon icon_dwn"></span></span>Download Now</a></li>
                                            <li><a href="#"><span className="opt_icon"><span className="icon icon_playlst"></span></span>Add To Playlist</a></li>
                                            <li><a href="#"><span className="opt_icon"><span className="icon icon_share"></span></span>Share</a></li>
                                        </ul>
                                    </li>
                                </ul>
                                <ul>
                                    <li><a href="#"><span className="play_no">2</span><span className="play_hover"></span></a></li>
                                    <li><Link to={`/artiste/show/`}>Cloud nine</Link></li>
                                    <li className="text-center"><a href="#">5:26</a></li>
                                    <li className="text-center"><a href="#"><span
                                        className="ms_icon1 ms_fav_icon"></span></a></li>
                                    <li className="text-center">78 373</li>
                                    <li className="text-center ms_more_icon"><a href="#"><span className="ms_icon1 ms_active_icon"></span></a>
                                        <ul className="more_option">
                                            <li><a href="#"><span className="opt_icon"><span className="icon icon_fav"></span></span>Add To Favourites</a></li>
                                            <li><a href="#"><span className="opt_icon"><span className="icon icon_queue"></span></span>Add To Queue</a></li>
                                            <li><a href="#"><span className="opt_icon"><span className="icon icon_dwn"></span></span>Download Now</a></li>
                                            <li><a href="#"><span className="opt_icon"><span className="icon icon_playlst"></span></span>Add To Playlist</a></li>
                                            <li><a href="#"><span className="opt_icon"><span className="icon icon_share"></span></span>Share</a></li>
                                        </ul>
                                    </li>
                                </ul>
                                <ul>
                                    <li><a href="#"><span className="play_no">3</span><span className="play_hover"></span></a></li>
                                    <li><Link to={`/artiste/show/`}>Cloud nine</Link></li>
                                    <li className="text-center"><a href="#">5:26</a></li>
                                    <li className="text-center"><a href="#"><span
                                        className="ms_icon1 ms_fav_icon"></span></a></li>
                                    <li className="text-center">2 373</li>
                                    <li className="text-center ms_more_icon"><a href="#"><span className="ms_icon1 ms_active_icon"></span></a>
                                        <ul className="more_option">
                                            <li><a href="#"><span className="opt_icon"><span className="icon icon_fav"></span></span>Add To Favourites</a></li>
                                            <li><a href="#"><span className="opt_icon"><span className="icon icon_queue"></span></span>Add To Queue</a></li>
                                            <li><a href="#"><span className="opt_icon"><span className="icon icon_dwn"></span></span>Download Now</a></li>
                                            <li><a href="#"><span className="opt_icon"><span className="icon icon_playlst"></span></span>Add To Playlist</a></li>
                                            <li><a href="#"><span className="opt_icon"><span className="icon icon_share"></span></span>Share</a></li>
                                        </ul>
                                    </li>
                                </ul>
                                <ul>
                                    <li><a href="#"><span className="play_no">4</span><span className="play_hover"></span></a></li>
                                    <li><Link to={`/artiste/show/`}>Cloud nine</Link></li>
                                    <li className="text-center"><a href="#">5:26</a></li>
                                    <li className="text-center"><a href="#"><span
                                        className="ms_icon1 ms_fav_icon"></span></a></li>
                                    <li className="text-center">1 373</li>
                                    <li className="text-center ms_more_icon"><a href="#"><span className="ms_icon1 ms_active_icon"></span></a>
                                        <ul className="more_option">
                                            <li><a href="#"><span className="opt_icon"><span className="icon icon_fav"></span></span>Add To Favourites</a></li>
                                            <li><a href="#"><span className="opt_icon"><span className="icon icon_queue"></span></span>Add To Queue</a></li>
                                            <li><a href="#"><span className="opt_icon"><span className="icon icon_dwn"></span></span>Download Now</a></li>
                                            <li><a href="#"><span className="opt_icon"><span className="icon icon_playlst"></span></span>Add To Playlist</a></li>
                                            <li><a href="#"><span className="opt_icon"><span className="icon icon_share"></span></span>Share</a></li>
                                        </ul>
                                    </li>
                                </ul>
                                <ul>
                                    <li><a href="#"><span className="play_no">5</span><span className="play_hover"></span></a></li>
                                    <li><a href="#">Cloud nine</a></li>
                                    <li className="text-center"><a href="#">5:26</a></li>
                                    <li className="text-center"><a href="#"><span
                                        className="ms_icon1 ms_fav_icon"></span></a></li>
                                    <li className="text-center">278 123</li>
                                    <li className="text-center ms_more_icon"><a href="#"><span className="ms_icon1 ms_active_icon"></span></a>
                                        <ul className="more_option">
                                            <li><a href="#"><span className="opt_icon"><span className="icon icon_fav"></span></span>Add To Favourites</a></li>
                                            <li><a href="#"><span className="opt_icon"><span className="icon icon_queue"></span></span>Add To Queue</a></li>
                                            <li><a href="#"><span className="opt_icon"><span className="icon icon_dwn"></span></span>Download Now</a></li>
                                            <li><a href="#"><span className="opt_icon"><span className="icon icon_playlst"></span></span>Add To Playlist</a></li>
                                            <li><a href="#"><span className="opt_icon"><span className="icon icon_share"></span></span>Share</a></li>
                                        </ul>
                                    </li>
                                </ul>
                                <ul>
                                    <li><a href="#"><span className="play_no">6</span><span className="play_hover"></span></a></li>
                                    <li><a href="#">Cloud nine</a></li>
                                    <li className="text-center"><a href="#">5:26</a></li>
                                    <li className="text-center"><a href="#"><span
                                        className="ms_icon1 ms_fav_icon"></span></a></li>
                                    <li className="text-center">278 465</li>
                                    <li className="text-center ms_more_icon"><a href="#"><span className="ms_icon1 ms_active_icon"></span></a>
                                        <ul className="more_option">
                                            <li><a href="#"><span className="opt_icon"><span className="icon icon_fav"></span></span>Add To Favourites</a></li>
                                            <li><a href="#"><span className="opt_icon"><span className="icon icon_queue"></span></span>Add To Queue</a></li>
                                            <li><a href="#"><span className="opt_icon"><span className="icon icon_dwn"></span></span>Download Now</a></li>
                                            <li><a href="#"><span className="opt_icon"><span className="icon icon_playlst"></span></span>Add To Playlist</a></li>
                                            <li><a href="#"><span className="opt_icon"><span className="icon icon_share"></span></span>Share</a></li>
                                        </ul>
                                    </li>
                                </ul>
                            </div>
                        </div>


                        <div className="ms_fea_album_slider">
                            <div className="ms_heading">
                                <h1>Autre projet de {music.user.name}</h1>
                            </div>

                            <AlbumUserInteresse {...this.props} />

                        </div>

                    </div>


                    <FooterMini/>

                    { /***Audio Player Section**/}


                </div>

                <FooterMultiple/>

            </>
        );
    }
}
AlbumShowSite.propTypes = {
    loadShowdata: PropTypes.func.isRequired,
};

const mapStoreToProps = store => ({
    music: getAlbumReselect(store),
});

export default connect(mapStoreToProps, {
    loadShowdata,unfavoriteItem,favoriteItem,
})(AlbumShowSite);

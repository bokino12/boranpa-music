import React, {Component} from 'react';
import HelmetSite from "../inc/vendor/HelmetSite";
import VerticalNaveSite from "../inc/VerticalNaveSite";
import HorizontaNaveSite from "../inc/HorizontaNaveSite";
import {Link} from "react-router-dom";
import FooterMultiple from "../inc/FooterMultiple";
import FooterMini from "../inc/FooterMini";
import UndoRedo from "../inc/vendor/UndoRedo";

class AlbumIndexSite extends Component {
    render() {
        return (
            <>
                <HelmetSite title={`Albums `}/>

                <div className="ms_main_wrapper">

                    <VerticalNaveSite/>

                    <div className="ms_content_wrapper padder_top80">

                        <HorizontaNaveSite/>


                        { /**Banner**/}
                        <div className="ms-banner">

                            {/**Featured Albumn Section Start**/}
                            <div className="ms_fea_album_slider">

                                <UndoRedo {...this.props}/>

                                <div className="ms_heading">
                                    <h1>Featured Albums</h1>
                                    <span className="veiw_all"><a href="#">view more</a></span>
                                </div>
                                <div className="row">
                                    <div className="col-md-3">
                                        <div className="ms_rcnt_box">
                                            <div className="ms_rcnt_box_img">
                                                <Link to={`/album/show/`}>
                                                    <img src="/assets/images/album/album1.jpg" alt=""/>
                                                </Link>
                                                <div className="ms_main_overlay">
                                                    <div className="ms_play_icon">
                                                        <Link to={`/album/show/`}><img src="/assets/images/svg/play.svg" alt=""/></Link>
                                                    </div>
                                                </div>
                                            </div>
                                            <div className="ms_rcnt_box_text">
                                                <h3>
                                                    <Link to={`/album/show/`}>LMF</Link>
                                                </h3>
                                                <p>Boclair Temgoua</p>
                                            </div>
                                        </div>
                                    </div>
                                    <div className="col-md-3">
                                        <div className="ms_rcnt_box">
                                            <div className="ms_rcnt_box_img">
                                                <img src="/assets/images/album/album2.jpg" alt=""/>
                                                <div className="ms_main_overlay">
                                                    <div className="ms_play_icon">
                                                        <a href="#"><img src="/assets/images/svg/play.svg" alt=""/></a>
                                                    </div>
                                                </div>
                                            </div>
                                            <div className="ms_rcnt_box_text">
                                                <h3><a href="#">Time flies</a></h3>
                                                <p>Ava Cornish & Brian Hill</p>
                                            </div>
                                        </div>
                                    </div>
                                    <div className="col-md-3">
                                        <div className="ms_rcnt_box">
                                            <div className="ms_rcnt_box_img">
                                                <img src="/assets/images/album/album3.jpg" alt=""/>
                                                <div className="ms_main_overlay">
                                                    <div className="ms_play_icon">
                                                        <a href="#"><img src="/assets/images/svg/play.svg" alt=""/></a>
                                                    </div>
                                                </div>
                                            </div>
                                            <div className="ms_rcnt_box_text">
                                                <h3><a href="#">Dark matters</a></h3>
                                                <p>Ava Cornish & Brian Hill</p>
                                            </div>
                                        </div>
                                    </div>
                                    <div className="col-md-3">
                                        <div className="ms_rcnt_box">
                                            <div className="ms_rcnt_box_img">
                                                <img src="/assets/images/album/album4.jpg" alt=""/>
                                                <div className="ms_main_overlay">
                                                    <div className="ms_play_icon">
                                                        <a href="#"><img src="/assets/images/svg/play.svg" alt=""/></a>
                                                    </div>
                                                </div>
                                            </div>
                                            <div className="ms_rcnt_box_text">
                                                <h3><a href="#">Eye to eye</a></h3>
                                                <p>Ava Cornish & Brian Hill</p>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            {/**Add Section Start**/}
                            <div className="ms_advr_wrapper">
                                <div className="container">
                                    <div className="row">
                                        <div className="col-lg-12">
                                            <a href="#"><img src="/assets/images/adv.jpg" alt="" className="img-fluid"/></a>
                                        </div>
                                    </div>
                                </div>
                            </div>


                        </div>

                       <FooterMini/>

                        { /***Audio Player Section**/}



                    </div>

                </div>

                <FooterMultiple/>

            </>
        );
    }
}

export default AlbumIndexSite;

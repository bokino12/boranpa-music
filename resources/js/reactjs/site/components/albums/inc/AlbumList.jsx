import React, {Component} from 'react';
import {Link} from "react-router-dom";
import MoreIcon from '../../../assets/images/svg/more.svg';
import PlayIcon from '../../../assets/images/svg/play.svg';

class AlbumList extends Component {
    render() {
        return (
            <div className="col-sm-3 col-md-3">
                <div className="ms_rcnt_box">
                    <div className="ms_rcnt_box_img">
                        <Link to={`/albums/${this.props.slugin}/${this.props.user.slug}/`}>
                            <img src={this.props.photo} alt={this.props.name}/>
                        </Link>
                        <div className="ms_main_overlay">
                            <div className="ms_play_icon">
                                <Link to={`/albums/${this.props.slugin}/${this.props.user.slug}/`}><img src={PlayIcon} alt={this.props.name} /></Link>
                            </div>
                        </div>
                    </div>
                    <div className="ms_rcnt_box_text">
                        <h3>{this.props.name}</h3>
                        <Link to={`/user/${this.props.user.slug}/`}>
                            <p>{this.props.user.name}</p>
                        </Link>
                    </div>
                </div>
            </div>
        );
    }
}

export default AlbumList;

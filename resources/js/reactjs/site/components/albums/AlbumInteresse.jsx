import React, {PureComponent} from 'react';
import {Link} from "react-router-dom";
import PropTypes from "prop-types";
import {connect} from "react-redux";
import {
    loadAlldatainter,
} from "../../redux/actions/albumAction";
import AlbumList from "./inc/AlbumList";
import {getAlbumsinterReselect} from "../../redux/selector";


class AlbumInteresse extends PureComponent {

    componentDidMount() {
        this.props.loadAlldatainter(this.props);
    }

    render() {
        const {musics} = this.props;
        const mapMusics = musics.length >= 0 ? (
            musics.map(item => {
                return (
                    <AlbumList key={item.id} {...item}  />
                )
            })
        ) : (
            <></>
        );
        return (
            <>
                <div className="ms_fea_album_slider">
                    <div className="ms_heading">
                        <h1>Featured Albums</h1>
                        <span className="veiw_all"><Link to={`/genres/`}>view more</Link></span>
                    </div>
                    <div className="row">

                        {mapMusics}

                    </div>
                </div>
            </>
        );
    }
}

AlbumInteresse.propTypes = {
    loadAlldatainter: PropTypes.func.isRequired,
};

const mapStoreToProps = store => ({
    musics: getAlbumsinterReselect(store),
});

export default connect(mapStoreToProps, {
    loadAlldatainter,
})(AlbumInteresse);

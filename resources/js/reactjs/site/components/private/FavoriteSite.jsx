import React, {Component} from 'react';
import HelmetSite from "../inc/vendor/HelmetSite";
import VerticalNaveSite from "../inc/VerticalNaveSite";
import HorizontaNaveSite from "../inc/HorizontaNaveSite";
import FooterMultiple from "../inc/FooterMultiple";
import {Link} from "react-router-dom";
import FooterMini from "../inc/FooterMini";
import UndoRedo from "../inc/vendor/UndoRedo";

class FavoriteSite extends Component {
    render() {
        return (
            <>
                <HelmetSite title={`Favorites `}/>


                <div className="ms_main_wrapper">


                    <HorizontaNaveSite/>

                    <VerticalNaveSite/>

                    <div className="ms_album_single_wrapper ms_artist_single">

                        <UndoRedo {...this.props}/>

                        <div className="ms_heading">
                            <h1>Your Favourites Title</h1>
                        </div>

                        <div className="album_inner_list">
                            <div className="album_list_wrapper">
                                <ul className="album_list_name">
                                    <li>#</li>
                                    <li>Title</li>
                                    <li>Auteur</li>
                                    <li className="text-center"></li>
                                    <li className="text-center"></li>
                                    <li className="text-center"></li>
                                    <li className="text-center"></li>
                                </ul>
                                <ul className="play_active_song">
                                    <li><a href="#"><span className="play_no">1</span><span className="play_hover"></span></a></li>
                                    <li><a href="#">Dark Alley Acoustic</a></li>
                                    <li><Link to={`/artiste/show/`}>Boclair Temgoua</Link></li>
                                    <li className="text-center"><a href="#">5:26</a></li>
                                    <li className="text-center"><a href="#"><span
                                        className="ms_icon1 ms_fav_icon"></span></a></li>
                                    <li className="text-center">1 222 3873</li>
                                </ul>
                                <ul>
                                    <li><a href="#"><span className="play_no">2</span><span className="play_hover"></span></a></li>
                                    <li><a href="#">Cloud nine</a></li>
                                    <li><Link to={`/artiste/show/`}>Boclair Temgoua</Link></li>
                                    <li className="text-center"><a href="#">5:26</a></li>
                                    <li className="text-center"><a href="#"><span
                                        className="ms_icon1 ms_fav_icon"></span></a></li>
                                    <li className="text-center">546 383</li>
                                </ul>
                                <ul>
                                    <li><a href="#"><span className="play_no">3</span><span className="play_hover"></span></a></li>
                                    <li><a href="#">Cloud nine</a></li>
                                    <li><Link to={`/artiste/show/`}>Boclair Temgoua</Link></li>
                                    <li className="text-center"><a href="#">5:26</a></li>
                                    <li className="text-center"><a href="#"><span
                                        className="ms_icon1 ms_fav_icon"></span></a></li>
                                    <li className="text-center">122 373</li>
                                </ul>
                                <ul>
                                    <li><a href="#"><span className="play_no">4</span><span className="play_hover"></span></a></li>
                                    <li><a href="#">Cloud nine</a></li>
                                    <li><Link to={`/artiste/show/`}>Boclair Temgoua</Link></li>
                                    <li className="text-center"><a href="#">5:26</a></li>
                                    <li className="text-center"><a href="#"><span
                                        className="ms_icon1 ms_fav_icon"></span></a></li>
                                    <li className="text-center">278 373</li>
                                </ul>
                                <ul>
                                    <li><a href="#"><span className="play_no">5</span><span className="play_hover"></span></a></li>
                                    <li><a href="#">Cloud nine</a></li>
                                    <li><Link to={`/artiste/show/`}>Boclair Temgoua</Link></li>
                                    <li className="text-center"><a href="#">5:26</a></li>
                                    <li className="text-center"><a href="#"><span
                                        className="ms_icon1 ms_fav_icon"></span></a></li>
                                    <li className="text-center">8 373</li>
                                </ul>
                                <ul>
                                    <li><a href="#"><span className="play_no">6</span><span className="play_hover"></span></a></li>
                                    <li><a href="#">Cloud nine</a></li>
                                    <li><Link to={`/artiste/show/`}>Kasse bokino</Link></li>
                                    <li className="text-center"><a href="#">5:26</a></li>
                                    <li className="text-center"><a href="#"><span
                                        className="ms_icon1 ms_fav_icon"></span></a></li>
                                    <li className="text-center">98 373</li>
                                </ul>

                                <div className="ms_view_more">
                                    <a href="#" className="ms_btn">view more</a>
                                </div>
                            </div>
                        </div>


                        <div className="ms_fea_album_slider">
                            <div className="row">
                                <div className="ms_heading">
                                    <h1>Your Favourites Albums</h1>
                                </div>
                                <div className="col-md-3">
                                    <div className="ms_rcnt_box">
                                        <div className="ms_rcnt_box_img">
                                            <Link to={`/album/show/`}>
                                                <img src="/assets/images/album/album1.jpg" alt=""/>
                                            </Link>
                                            <div className="ms_main_overlay">
                                                <a href="#">
                                                    <div className="ms_more_icon">
                                                        <img src="/assets/images/svg/more.svg" alt=""/>
                                                    </div>
                                                </a>
                                                <div className="ms_play_icon">
                                                    <Link to={`/album/show/`}><img src="/assets/images/svg/play.svg" alt=""/></Link>
                                                </div>
                                            </div>
                                        </div>
                                        <div className="ms_rcnt_box_text">
                                            <h3>
                                                <Link to={`/artiste/show/`}>Boclair Temgoua</Link> - <Link to={`/album/show/`}> Force Trankile</Link>
                                            </h3>
                                            <p>2018</p>
                                        </div>
                                    </div>
                                </div>
                                <div className="col-md-3">
                                    <div className="ms_rcnt_box">
                                        <div className="ms_rcnt_box_img">
                                            <img src="/assets/images/album/album2.jpg" alt=""/>
                                            <div className="ms_main_overlay">
                                                <a href="#">
                                                    <div className="ms_more_icon">
                                                        <img src="/assets/images/svg/add_q.svg" alt=""/>
                                                    </div>
                                                </a>
                                                <div className="ms_play_icon">
                                                    <a href="#"><img src="/assets/images/svg/play.svg" alt=""/></a>
                                                </div>
                                            </div>
                                        </div>
                                        <div className="ms_rcnt_box_text">
                                            <h3><a href="#">Boclair Temgoua - FDP</a></h3>
                                            <p>2015</p>
                                        </div>
                                    </div>
                                </div>
                                <div className="col-md-3">
                                    <div className="ms_rcnt_box">
                                        <div className="ms_rcnt_box_img">
                                            <img src="/assets/images/album/album3.jpg" alt=""/>
                                            <div className="ms_main_overlay">
                                                <a href="#">
                                                    <div className="ms_more_icon">
                                                        <img src="/assets/images/svg/more.svg" alt=""/>
                                                    </div>
                                                </a>
                                                <div className="ms_play_icon">
                                                    <a href="#"><img src="/assets/images/svg/play.svg" alt=""/></a>
                                                </div>
                                            </div>
                                        </div>
                                        <div className="ms_rcnt_box_text">
                                            <h3><a href="#">Boclair Temgoua - QLF</a></h3>
                                            <p>2013</p>
                                        </div>
                                    </div>
                                </div>
                                <div className="col-md-3">
                                    <div className="ms_rcnt_box">
                                        <div className="ms_rcnt_box_img">
                                            <img src="/assets/images/album/album4.jpg" alt=""/>
                                            <div className="ms_main_overlay">
                                                <a href="#">
                                                    <div className="ms_more_icon">
                                                        <img src="/assets/images/svg/add_q.svg" alt=""/>
                                                    </div>
                                                </a>
                                                <div className="ms_play_icon">
                                                    <a href="#"><img src="/assets/images/svg/play.svg" alt=""/></a>
                                                </div>
                                            </div>
                                        </div>
                                        <div className="ms_rcnt_box_text">
                                            <h3><a href="#">Freeze corneole - LMF</a></h3>
                                            <p>2020</p>
                                        </div>
                                    </div>
                                </div>

                                <div className="ms_view_more">
                                    <a href="#" className="ms_btn">view more</a>
                                </div>
                            </div>
                        </div>

                        <div className="ms_fea_album_slider">
                            <div className="row">
                                <div className="ms_heading">
                                    <h1>Your Favourites Radios</h1>
                                </div>
                                <div className="col-md-3">
                                    <div className="ms_rcnt_box">
                                        <div className="ms_rcnt_box_img">
                                            <Link to={`/album/show/`}>
                                                <img src="/assets/images/album/album1.jpg" alt=""/>
                                            </Link>
                                            <div className="ms_main_overlay">
                                                <a href="#">
                                                    <div className="ms_more_icon">
                                                        <img src="/assets/images/svg/more.svg" alt=""/>
                                                    </div>
                                                </a>
                                                <div className="ms_play_icon">
                                                    <Link to={`/album/show/`}><img src="/assets/images/svg/play.svg" alt=""/></Link>
                                                </div>
                                            </div>
                                        </div>
                                        <div className="ms_rcnt_box_text">
                                            <h3>
                                                <Link to={`/artiste/show/`}>Boclair Temgoua</Link> - <Link to={`/album/show/`}> Force Trankile</Link>
                                            </h3>
                                            <p>2018</p>
                                        </div>
                                    </div>
                                </div>
                                <div className="col-md-3">
                                    <div className="ms_rcnt_box">
                                        <div className="ms_rcnt_box_img">
                                            <img src="/assets/images/album/album2.jpg" alt=""/>
                                            <div className="ms_main_overlay">
                                                <a href="#">
                                                    <div className="ms_more_icon">
                                                        <img src="/assets/images/svg/add_q.svg" alt=""/>
                                                    </div>
                                                </a>
                                                <div className="ms_play_icon">
                                                    <a href="#"><img src="/assets/images/svg/play.svg" alt=""/></a>
                                                </div>
                                            </div>
                                        </div>
                                        <div className="ms_rcnt_box_text">
                                            <h3><a href="#">Boclair Temgoua - FDP</a></h3>
                                            <p>2015</p>
                                        </div>
                                    </div>
                                </div>
                                <div className="col-md-3">
                                    <div className="ms_rcnt_box">
                                        <div className="ms_rcnt_box_img">
                                            <img src="/assets/images/album/album3.jpg" alt=""/>
                                            <div className="ms_main_overlay">
                                                <a href="#">
                                                    <div className="ms_more_icon">
                                                        <img src="/assets/images/svg/more.svg" alt=""/>
                                                    </div>
                                                </a>
                                                <div className="ms_play_icon">
                                                    <a href="#"><img src="/assets/images/svg/play.svg" alt=""/></a>
                                                </div>
                                            </div>
                                        </div>
                                        <div className="ms_rcnt_box_text">
                                            <h3><a href="#">Boclair Temgoua - QLF</a></h3>
                                            <p>2013</p>
                                        </div>
                                    </div>
                                </div>
                                <div className="col-md-3">
                                    <div className="ms_rcnt_box">
                                        <div className="ms_rcnt_box_img">
                                            <img src="/assets/images/album/album4.jpg" alt=""/>
                                            <div className="ms_main_overlay">
                                                <a href="#">
                                                    <div className="ms_more_icon">
                                                        <img src="/assets/images/svg/add_q.svg" alt=""/>
                                                    </div>
                                                </a>
                                                <div className="ms_play_icon">
                                                    <a href="#"><img src="/assets/images/svg/play.svg" alt=""/></a>
                                                </div>
                                            </div>
                                        </div>
                                        <div className="ms_rcnt_box_text">
                                            <h3><a href="#">Freeze corneole - LMF</a></h3>
                                            <p>2020</p>
                                        </div>
                                    </div>
                                </div>

                                <div className="ms_view_more">
                                    <a href="#" className="ms_btn">view more</a>
                                </div>
                            </div>
                        </div>


                    </div>


                    <FooterMini/>

                    { /***Audio Player Section**/}



                </div>

                <FooterMultiple/>

            </>
        );
    }
}

export default FavoriteSite;

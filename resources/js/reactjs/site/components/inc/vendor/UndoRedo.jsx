import React from "react";

export default function UndoRedo(props){
    return(
       <>
           <a href={void(0)} style={{cursor:"pointer"}} onClick={props.history.goBack}><i className="fa fa-undo"/></a><br/><br/>
       </>
    )
}

import React from "react";
import MoreIcon from '../../../assets/images/svg/more.svg';
import AddQIcon from '../../../assets/images/svg/add_q.svg';
export default function ButtonFavorite(props) {
    return (
        <>
            {props.favoriteted ?

                <a href={`#${props.slug}`}  onClick={() => props.unfavoriteItem(props)} className="ms_btn">
                      <span className="play_all">
                                            <img src={MoreIcon} alt=""/>Suivi</span>
                </a>

                :

                <a href={`#${props.slug}`} onClick={() => props.favoriteItem(props)} className="ms_btn">
                      <span className="play_all">
                                            <img src={AddQIcon} alt=""/>Suivre</span>
                </a>
            }

        </>
    )
}

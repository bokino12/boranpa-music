import React, {Component} from 'react';

class FooterMultiple extends Component {
    render() {
        return (
            <>
                { /***Register Modal Start**/
                    /**Modal**/}
                <div className="ms_register_popup">
                    <div id="myModal" className="modal  centered-modal" role="dialog">
                        <div className="modal-dialog register_dialog">

                            <div className="modal-content">
                                <button type="button" className="close" data-dismiss="modal">
                                    <i className="fa_icon form_close"></i>
                                </button>
                                <div className="modal-body">
                                    <div className="ms_register_img">
                                        <img src="images/register_img.png" alt="" className="img-fluid" />
                                    </div>
                                    <div className="ms_register_form">
                                        <h2>Register / Sign Up</h2>
                                        <div className="form-group">
                                            <input type="text" placeholder="Enter Your Name" className="form-control"/>
                                            <span className="form_icon">
							<i className="fa_icon form-user" aria-hidden="true"></i>
							</span>
                                        </div>
                                        <div className="form-group">
                                            <input type="text" placeholder="Enter Your Email" className="form-control"/>
                                            <span className="form_icon">
							<i className="fa_icon form-envelope" aria-hidden="true"></i>
						</span>
                                        </div>
                                        <div className="form-group">
                                            <input type="password" placeholder="Enter Password" className="form-control"/>
                                            <span className="form_icon">
						<i className="fa_icon form-lock" aria-hidden="true"></i>
						</span>
                                        </div>
                                        <div className="form-group">
                                            <input type="password" placeholder="Confirm Password" className="form-control"/>
                                            <span className="form_icon">
						<i className=" fa_icon form-lock" aria-hidden="true"></i>
						</span>
                                        </div>
                                        <a href="#" className="ms_btn">register now</a>
                                        <p>Already Have An Account? <a href="#myModal1" data-toggle="modal" className="ms_modal hideCurrentModel">login here</a></p>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    { /***Login Popup Start**/}
                    <div id="myModal1" className="modal  centered-modal" role="dialog">
                        <div className="modal-dialog login_dialog">

                            <div className="modal-content">
                                <button type="button" className="close" data-dismiss="modal">
                                    <i className="fa_icon form_close"></i>
                                </button>
                                <div className="modal-body">
                                    <div className="ms_register_img">
                                        <img src="/assets/images/register_img.png" alt="" className="img-fluid" />
                                    </div>
                                    <div className="ms_register_form">
                                        <h2>login / Sign in</h2>
                                        <div className="form-group">
                                            <input type="text" placeholder="Enter Your Email" className="form-control"/>
                                            <span className="form_icon">
							<i className="fa_icon form-envelope" aria-hidden="true"></i>
						</span>
                                        </div>
                                        <div className="form-group">
                                            <input type="password" placeholder="Enter Password" className="form-control"/>
                                            <span className="form_icon">
						<i className="fa_icon form-lock" aria-hidden="true"></i>
						</span>
                                        </div>
                                        <div className="remember_checkbox">
                                            <label>Keep me signed in
                                                <input type="checkbox"/>
                                                <span className="checkmark"></span>
                                            </label>
                                        </div>
                                        <a href="profile.html" className="ms_btn" target="_blank">login now</a>
                                        <div className="popup_forgot">
                                            <a href="#">Forgot Password ?</a>
                                        </div>
                                        <p>Don't Have An Account? <a href="#myModal" data-toggle="modal" className="ms_modal1 hideCurrentModel">register here</a></p>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                {/***Language Selection Modal**/}
                <div className="ms_lang_popup">
                    <div id="lang_modal" className="modal  centered-modal" role="dialog">
                        <div className="modal-dialog">

                            <div className="modal-content">
                                <button type="button" className="close" data-dismiss="modal">
                                    <i className="fa_icon form_close"></i>
                                </button>
                                <div className="modal-body">
                                    <h1>language selection</h1>
                                    <p>Please select the language(s) of the music you listen to.</p>
                                    <ul className="lang_list">
                                        <li>
                                            <label className="lang_check_label">
                                                English
                                                <input type="checkbox" name="check"/>
                                                <span className="label-text"></span>
                                            </label>
                                        </li>

                                        <li>
                                            <label className="lang_check_label">
                                                French
                                                <input type="checkbox" name="check"/>
                                                <span className="label-text"></span>
                                            </label>
                                        </li>
                                    </ul>
                                    <div className="ms_lang_btn">
                                        <a href="#" className="ms_btn">apply</a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                {/***Queue Clear Model**/}
                <div className="ms_clear_modal">
                    <div id="clear_modal" className="modal  centered-modal" role="dialog">
                        <div className="modal-dialog">

                            <div className="modal-content">
                                <button type="button" className="close" data-dismiss="modal">
                                    <i className="fa_icon form_close"></i>
                                </button>
                                <div className="modal-body">
                                    <h1>Are you sure you want to clear your queue?</h1>
                                    <div className="clr_modal_btn">
                                        <a href="#">clear all</a>
                                        <a href="#">cancel</a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                {/***Queue Save Modal**/}
                <div className="ms_save_modal">
                    <div id="save_modal" className="modal  centered-modal" role="dialog">
                        <div className="modal-dialog">

                            <div className="modal-content">
                                <button type="button" className="close" data-dismiss="modal">
                                    <i className="fa_icon form_close"></i>
                                </button>
                                <div className="modal-body">
                                    <h1>Log in to start sharing your music!</h1>
                                    <div className="save_modal_btn">
                                        <a href="#"><i className="fa fa-google-plus-square" aria-hidden="true"></i> continue with google </a>
                                        <a href="#"><i className="fa fa-facebook-square" aria-hidden="true"></i> continue with facebook</a>
                                    </div>
                                    <div className="ms_save_email">
                                        <h3>or use your email</h3>
                                        <div className="save_input_group">
                                            <input type="text" placeholder="Enter Your Name" className="form-control"/>
                                        </div>
                                        <div className="save_input_group">
                                            <input type="password" placeholder="Enter Password" className="form-control"/>
                                        </div>
                                        <button className="save_btn">Log in</button>
                                    </div>
                                    <div className="ms_dnt_have">
                                        <span>Dont't have an account ?</span>
                                        <a href="#" className="hideCurrentModel" data-toggle="modal" data-target="#myModal">Register Now</a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </>
        );
    }
}

export default FooterMultiple;

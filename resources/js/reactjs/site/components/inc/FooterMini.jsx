import React, {Component} from 'react';

class FooterMini extends Component {
    render() {
        return (
            <>
                {/**Footer Start**/}
                <div className="ms_footer_wrapper">

                    { /***Copyright**/}
                    <div className="col-lg-12">
                        <div className="ms_copyright">
                            <div className="footer_border"></div>
                            <p><a target="_blank" href="https://www.templateshub.net">Templates Hub</a></p>
                        </div>
                    </div>
                </div>
            </>
        );
    }
}

export default FooterMini;

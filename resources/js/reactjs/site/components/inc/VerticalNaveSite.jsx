import React from 'react'
import {Link, NavLink} from "react-router-dom";

function VerticalNaveSite() {

    return(

        <div className="ms_sidemenu_wrapper">
            <div className="ms_nav_close">
                <i className="fa fa-angle-right" aria-hidden="true"/>
            </div>
            <div className="ms_sidemenu_inner">
                <div className="ms_logo_inner">
                    <div className="ms_logo">
                        <Link to={`/`}>
                            <img src="/assets/images/logo.png" alt="" className="img-fluid"/>
                        </Link>
                    </div>
                    <div className="ms_logo_open">
                        <Link to={`/`}>
                            <img src="/assets/images/open_logo.png" alt="" className="img-fluid"/>
                        </Link>
                    </div>
                </div>
                <div className="ms_nav_wrapper">
                    <ul>
                        <li><NavLink  to={`/home/`}  title="Discover">
						<span className="nav_icon">
							<span className="icon icon_discover"/>
						</span>
                            <span className="nav_text">
							discover
						</span>
                        </NavLink>
                        </li>
                        <li><NavLink to={`/albums/`} title="Albums">
						<span className="nav_icon">
							<span className="icon icon_albums"/>
						</span>
                            <span className="nav_text">
							albums
						</span>
                        </NavLink>
                        </li>
                        <li><NavLink to={`/artistes/`} title="Artists">
						<span className="nav_icon">
							<span className="icon icon_artists"/>
						</span>
                            <span className="nav_text">
							artists
						</span>
                        </NavLink>
                        </li>
                        <li><NavLink  to={`/genres/`} title="Genres">
						<span className="nav_icon">
							<span className="icon icon_genres"/>
						</span>
                            <span className="nav_text">
							genres
						</span>
                        </NavLink>
                        </li>
                        <li>
                            <NavLink to={`/statistics/country/`} title="Top Tracks">
						<span className="nav_icon">
							<span className="icon icon_tracks"></span>
						</span>
                            <span className="nav_text">
							top tracks
						</span>
                        </NavLink>
                        </li>
                        <li><a href="#" title="Free Music">
						<span className="nav_icon">
							<span className="icon icon_music"></span>
						</span>
                            <span className="nav_text">
							free music
						</span>
                        </a>
                        </li>
                        <li><NavLink to={`/radios/`} title="Stations">
						<span className="nav_icon">
							<span className="icon icon_station"/>
						</span>
                            <span className="nav_text">
							stations
						</span>
                        </NavLink>
                        </li>
                    </ul>
                    <ul className="nav_playlist">

                        <li><NavLink to={`/favorites/`} title="Favourites">
						<span className="nav_icon">
							<span className="icon icon_favourite"/>
						</span>
                            <span className="nav_text">
							favourites
						</span>
                        </NavLink>
                        </li><li><a href="#" title="Featured Playlist">
						<span className="nav_icon">
							<span className="icon icon_fe_playlist"></span>
						</span>
                        <span className="nav_text">
							featured playlist
						</span>
                    </a>
                    </li>
                    </ul>
                </div>
            </div>
        </div>
    )


}

export default VerticalNaveSite;

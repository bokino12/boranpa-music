import React, {PureComponent} from 'react';
import {Link} from "react-router-dom";
import PlayIcon from '../../../assets/images/svg/play.svg';

class GenreList extends PureComponent {
    render() {
        return (
            <div className="col-sm-3 mx-auto">
                <div className="ms_genres_box">
                    <Link to={`/genres/${this.props.slug}/`}><img src={this.props.photo} alt={this.props.name} className="img-fluid"/></Link>
                    <div className="ms_main_overlay">
                        <div className="ms_play_icon">
                            <img src={PlayIcon} alt={this.props.name}/>
                        </div>
                        <div className="ovrly_text_div">
                            <span className="ovrly_text1"><Link to={`/genres/${this.props.slug}/`}>{this.props.name}</Link></span>
                            <span className="ovrly_text2"><Link to={`/genres/${this.props.slug}/`}>view song</Link></span>
                        </div>
                    </div>
                    <div className="ms_box_overlay_on">
                        <div className="ovrly_text_div">
                            <span className="ovrly_text1"><Link to={`/genres/${this.props.slug}/`}>{this.props.name}</Link></span>
                            <span className="ovrly_text2"><Link to={`/genres/${this.props.slug}/`}>view song</Link></span>
                        </div>
                    </div>
                </div>
            </div>
        );
    }
}

export default GenreList;

import React, {Component} from 'react';
import HelmetSite from "../inc/vendor/HelmetSite";
import VerticalNaveSite from "../inc/VerticalNaveSite";
import HorizontaNaveSite from "../inc/HorizontaNaveSite";
import {Link} from "react-router-dom";
import FooterMultiple from "../inc/FooterMultiple";
import FooterMini from "../inc/FooterMini";
import UndoRedo from "../inc/vendor/UndoRedo";
import PropTypes from "prop-types";
import {getGenreReselect} from "../../redux/selector";
import {connect} from "react-redux";
import {loadShowdata} from "../../redux/actions/genreAction";
import ReadMoreAndLess from "react-read-more-less";
import GenreInteresse from "./GenreInteresse";

import PlayAllIcon from '../../assets/images/svg/play_all.svg';

class GenresShowSite extends Component {

    componentDidMount() {
        this.props.loadShowdata(this.props);
    }

    render() {
        const {genre} = this.props;
        return (
            <>
                <HelmetSite title={`${genre.name || $name_site} | ${$name_site}  `}/>

                <div className="ms_main_wrapper">



                    <HorizontaNaveSite/>
                    <VerticalNaveSite/>

                    <div className="ms_album_single_wrapper">

                        <UndoRedo {...this.props}/>

                        <div className="album_single_data">
                            <div className="album_single_img">
                                <img src={genre.photo} alt={genre.name} className="img-fluid"/>
                            </div>

                            {genre.slug && (
                                <div className="album_single_text">
                                    <h2>{genre.name}</h2>
                                    <div className="album_btn">
                                        <div className="about_artist">
                                            <ReadMoreAndLess
                                                className="read-more-content"
                                                charLimit={100}
                                                readMoreText="+"
                                                readLessText="--"
                                            >
                                                {genre.description || ""}
                                            </ReadMoreAndLess>
                                        </div>
                                        <br/>
                                        <a href="#" className="ms_btn play_btn">
                                        <span className="play_all">
                                            <img src={PlayAllIcon} alt=""/>Play All</span>
                                        </a>
                                        {/*

                                    <a href="#" className="ms_btn play_btn">
                                           <span className="play_all">
                                            <img src="/assets/images/svg/pause_all.svg" alt=""/>Pause</span>
                                    </a>
                                    */}
                                    </div>
                                </div>
                            )}
                        </div>

                        <div className="album_inner_list">
                            <div className="album_list_wrapper">
                                <ul className="album_list_name">
                                    <li>#</li>
                                    <li>Title</li>
                                    <li>Album</li>
                                    <li>Auteur</li>
                                    <li className="text-center"></li>
                                    <li className="text-center"></li>
                                    <li className="text-center"></li>
                                </ul>
                                <ul className="play_active_song">
                                    <li><a href="#"><span className="play_no">1</span><span className="play_hover"></span></a></li>
                                    <li><a href="#">Dark Alley Acoustic</a></li>
                                    <li><Link to={`/album/show/`}>LMF</Link></li>
                                    <li><Link to={`/artiste/show/`}>Temgoua</Link></li>
                                    <li className="text-center"><a href="#">5:26</a></li>
                                    <li className="text-center"><a href="#"><span
                                        className="ms_icon1 ms_fav_icon"></span></a></li>
                                </ul>
                                <ul>
                                    <li><a href="#"><span className="play_no">2</span><span className="play_hover"></span></a></li>
                                    <li><a href="#">Cloud nine</a></li>
                                    <li><Link to={`/album/show/`}>QLF</Link></li>
                                    <li><Link to={`/artiste/show/`}>Bokino 238</Link></li>
                                    <li className="text-center"><a href="#">5:26</a></li>
                                    <li className="text-center"><a href="#"><span
                                        className="ms_icon1 ms_fav_icon"></span></a></li>
                                </ul>
                                <ul>
                                    <li><a href="#"><span className="play_no">3</span><span className="play_hover"></span></a></li>
                                    <li><a href="#">Cloud nine</a></li>
                                    <li><Link to={`/album/show/`}>QLF</Link></li>
                                    <li><Link to={`/artiste/show/`}>Kasse Gabin</Link></li>
                                    <li className="text-center"><a href="#">5:26</a></li>
                                    <li className="text-center"><a href="#"><span
                                        className="ms_icon1 ms_fav_icon"></span></a></li>
                                </ul>
                                <ul>
                                    <li><a href="#"><span className="play_no">4</span><span className="play_hover"></span></a></li>
                                    <li><a href="#">Cloud nine</a></li>
                                    <li><Link to={`/album/show/`}>Menace Fantore...</Link></li>
                                    <li><Link to={`/artiste/show/`}>Boclair Temgoua</Link></li>
                                    <li className="text-center"><a href="#">5:26</a></li>
                                    <li className="text-center"><a href="#"><span
                                        className="ms_icon1 ms_fav_icon"></span></a></li>
                                </ul>
                                <ul>
                                    <li><a href="#"><span className="play_no">5</span><span className="play_hover"></span></a></li>
                                    <li><a href="#">Cloud nine</a></li>
                                    <li><Link to={`/album/show/`}>LMF</Link></li>
                                    <li><Link to={`/artiste/show/`}>Boclair Temgoua</Link></li>
                                    <li className="text-center"><a href="#">5:26</a></li>
                                    <li className="text-center"><a href="#"><span
                                        className="ms_icon1 ms_fav_icon"></span></a></li>
                                </ul>
                                <ul>
                                    <li><a href="#"><span className="play_no">6</span><span className="play_hover"></span></a></li>
                                    <li><a href="#">Cloud nine</a></li>
                                    <li><Link to={`/album/show/`}>LMF</Link></li>
                                    <li><Link to={`/artiste/show/`}>Kasse bokino</Link></li>
                                    <li className="text-center"><a href="#">5:26</a></li>
                                    <li className="text-center"><a href="#"><span
                                        className="ms_icon1 ms_fav_icon"></span></a></li>
                                </ul>

                                <div className="ms_view_more">
                                    <a href="#" className="ms_btn">view more</a>
                                </div>
                            </div>
                        </div>


                        {/**Top Genres Section Start**/}
                       <GenreInteresse {...this.props} />

                    </div>


                    <FooterMini/>

                    { /***Audio Player Section**/}


                </div>

                <FooterMultiple/>

            </>
        );
    }
}

GenresShowSite.propTypes = {
    loadShowdata: PropTypes.func.isRequired,
};

const mapStoreToProps = store => ({
    genre: getGenreReselect(store),
});

export default connect(mapStoreToProps, {
    loadShowdata,
})(GenresShowSite);

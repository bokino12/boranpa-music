import React, {PureComponent} from 'react';
import {Link} from "react-router-dom";
import PropTypes from "prop-types";
import {connect} from "react-redux";
import {
    loadAlldatainter,
} from "../../redux/actions/genreAction";
import GenreList from "./inc/GenreList";


class GenreInteresse extends PureComponent {

    componentDidMount() {
        this.props.loadAlldatainter(this.props);
    }

    render() {
        const {genres} = this.props;
        const mapGenres = genres.length >= 0 ? (
            genres.map(item => {
                return (
                    <GenreList key={item.id} {...item}  />
                )
            })
        ) : (
            <></>
        );
        return (
            <>
                <div className="ms_genres_wrapper">
                    <div className="row">
                        <div className="col-lg-12">
                            <div className="ms_heading">
                                <h1>Top Genres</h1>
                                <span className="veiw_all"><Link to={`/genres/`}>view more</Link></span>
                            </div>
                        </div>

                        {mapGenres}

                    </div>
                </div>
            </>
        );
    }
}

GenreInteresse.propTypes = {
    loadAlldatainter: PropTypes.func.isRequired,
};

const mapStoreToProps = store => ({
    genres: store.genres.items,
});

export default connect(mapStoreToProps, {
    loadAlldatainter,
})(GenreInteresse);

import React, {Component} from 'react';
import HelmetSite from "../inc/vendor/HelmetSite";
import VerticalNaveSite from "../inc/VerticalNaveSite";
import HorizontaNaveSite from "../inc/HorizontaNaveSite";
import FooterMini from "../inc/FooterMini";
import FooterMultiple from "../inc/FooterMultiple";
import UndoRedo from "../inc/vendor/UndoRedo";
import PropTypes from "prop-types";
import {getGenresReselect} from "../../redux/selector";
import {connect} from "react-redux";
import {loadAlldata} from "../../redux/actions/genreAction";
import GenreList from "./inc/GenreList";

class GenresIndexSite extends Component {


    componentDidMount() {
        this.props.loadAlldata(this.props);
    }

    render() {
        const {genres} = this.props;
        const mapGenres = genres.length >= 0 ? (
            genres.map(item => {
                return (
                    <GenreList key={item.id} {...item}  />
                )
            })
        ) : (
            <></>
        );
        return (
            <>
                <HelmetSite title={`Genre | ${$name_site}`}/>

                <div className="ms_main_wrapper">

                    <VerticalNaveSite/>

                    <div className="ms_content_wrapper padder_top80">


                        <HorizontaNaveSite/>


                        { /**Banner**/}
                        <div className="ms-banner">

                            {/**Top Genres Section Start**/}
                            <div className="ms_genres_wrapper">

                                <UndoRedo {...this.props}/>

                                <div className="row">
                                    <div className="col-lg-12">
                                        <div className="ms_heading">
                                            <h1>Genres</h1>
                                        </div>
                                    </div>

                                    {mapGenres}

                                </div>
                            </div>

                        </div>

                        <FooterMini/>

                        { /***Audio Player Section**/}



                    </div>

                </div>

                <FooterMultiple/>

            </>
        );
    }
}
GenresIndexSite.propTypes = {
    loadAlldata: PropTypes.func.isRequired,
};

const mapStoreToProps = store => ({
    genres: getGenresReselect(store),
});

export default connect(mapStoreToProps, {
    loadAlldata,
})(GenresIndexSite);

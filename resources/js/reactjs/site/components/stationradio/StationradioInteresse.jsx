import React, {Component} from 'react';
import {Link} from "react-router-dom";
import PropTypes from "prop-types";
import {connect} from "react-redux";
import {
    loadAllstationradiointer,
} from "../../redux/actions/stationradioAction";
import StationradioList from "./inc/StationradioList";


class StationradioInteresse extends Component {

    componentDidMount() {
        this.props.loadAllstationradiointer(this.props);
    }

    render() {
        const {stationradios} = this.props;
        const mapStationradios = stationradios.length >= 0 ? (
            stationradios.map(item => {
                return (
                    <StationradioList key={item.id} {...item}  />
                )
            })
        ) : (
            <></>
        );
        return (
            <>
                <div className="ms_fea_album_slider">
                    <div className="row">
                        <div className="col-lg-12">
                            <div className="ms_heading">
                                <h1>Live Stations</h1>
                                <span className="veiw_all"><Link to={`/radios/`}>view more</Link></span>
                            </div>
                        </div>

                        {mapStationradios}

                    </div>
                </div>
            </>
        );
    }
}

StationradioInteresse.propTypes = {
    loadAllstationradiointer: PropTypes.func.isRequired,
};

const mapStoreToProps = store => ({
    stationradios: store.stationradios.items,
});

export default connect(mapStoreToProps, {
    loadAllstationradiointer,
})(StationradioInteresse);

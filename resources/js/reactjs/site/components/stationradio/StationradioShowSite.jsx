import React, {Component} from 'react';
import HelmetSite from "../inc/vendor/HelmetSite";
import VerticalNaveSite from "../inc/VerticalNaveSite";
import HorizontaNaveSite from "../inc/HorizontaNaveSite";
import {Link} from "react-router-dom";
import LazyLoad from "react-lazyload";
import ReadMoreAndLess from "react-read-more-less";
import FooterMini from "../inc/FooterMini";
import FooterMultiple from "../inc/FooterMultiple";
import UndoRedo from "../inc/vendor/UndoRedo";
import PropTypes from "prop-types";
import {getStationradioReselect} from "../../redux/selector";
import {connect} from "react-redux";
import {loadShowstationradio,unfavoriteItem,favoriteItem} from "../../redux/actions/stationradioAction";
import StationradioInteresse from "./StationradioInteresse";
import ButtonFavorite from "../inc/vendor/ButtonFavorite";
const abbrev = ['', 'k', 'M', 'B', 'T'];
import PlayAllIcon from '../../assets/images/svg/play_all.svg';
import PauseAllIcon from '../../assets/images/svg/pause_all.svg';

class StationradioShowSite extends Component {

    componentDidMount() {
        this.props.loadShowstationradio(this.props);
    }


    data_countfavoritesFormatter(countfavorites, precision) {
        const unrangifiedOrder = Math.floor(Math.log10(Math.abs(countfavorites)) / 3);
        const order = Math.max(0, Math.min(unrangifiedOrder, abbrev.length -1 ));
        const suffix = abbrev[order];
        return (countfavorites / Math.pow(10, order * 3)).toFixed(precision) + suffix;
    }
    render() {
        const {stationradio} = this.props;
        return (
            <>
                <HelmetSite title={`${stationradio.name || $name_site} | ${$name_site}  `}/>

                <div className="ms_main_wrapper">



                    <HorizontaNaveSite/>

                    <VerticalNaveSite/>

                    <div className="ms_album_single_wrapper ms_artist_single">

                        <UndoRedo {...this.props}/>

                        <div className="album_single_data">
                            <div className="album_single_img">
                                <LazyLoad>
                                    <img src={stationradio.photo} alt={stationradio.name} className="img-fluid"/>
                                </LazyLoad>
                            </div>
                            {stationradio.slug && (
                                <div className="album_single_text">
                                    <h2>{stationradio.name}</h2>
                                    <p className="singer_name">{this.data_countfavoritesFormatter(stationradio.countfavorites || "")} {stationradio.countfavorites > 1 ? "abonnés" : "abonné"}</p>
                                    <p className="singer_name">{stationradio.city} - Cameroun</p>
                                    <div className="about_artist">
                                        <ReadMoreAndLess
                                            className="read-more-content"
                                            charLimit={100}
                                            readMoreText="+"
                                            readLessText="--"
                                        >
                                            {stationradio.description || ""}
                                        </ReadMoreAndLess>
                                    </div>
                                    <div className="album_btn">
                                        <audio src={stationradio.link_red} />


                                        <a href="#" className="ms_btn play_btn">
                                        <span className="play_all">
                                            <img src={PlayAllIcon} alt=""/>Play</span>
                                        </a>


                                        {/*
                                    <a href="#" className="ms_btn play_btn">
                                           <span className="play_all">
                                            <img src={PauseAllIcon} alt=""/>Pause</span>
                                    </a>
                                    */}

                                        <ButtonFavorite  {...this.props} {...stationradio}
                                                         unfavoriteItem={this.props.unfavoriteItem}
                                                         favoriteItem={this.props.favoriteItem} />

                                    </div>
                                </div>
                            )}
                        </div>


                       <StationradioInteresse {...this.props}/>

                    </div>


                    <FooterMini/>

                    { /***Audio Player Section**/}


                </div>

                <FooterMultiple/>

            </>
        );
    }
}
StationradioShowSite.propTypes = {
    loadShowstationradio: PropTypes.func.isRequired,
};

const mapStoreToProps = store => ({
    stationradio: getStationradioReselect(store),
});

export default connect(mapStoreToProps, {
    loadShowstationradio,unfavoriteItem,favoriteItem
})(StationradioShowSite);

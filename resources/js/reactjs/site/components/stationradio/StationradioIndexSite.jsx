import React, {Component} from 'react';
import HelmetSite from "../inc/vendor/HelmetSite";
import VerticalNaveSite from "../inc/VerticalNaveSite";
import HorizontaNaveSite from "../inc/HorizontaNaveSite";
import {Link} from "react-router-dom";
import FooterMini from "../inc/FooterMini";
import FooterMultiple from "../inc/FooterMultiple";
import UndoRedo from "../inc/vendor/UndoRedo";
import moment from "moment";
import PropTypes from "prop-types";
import {connect} from "react-redux";
import {
    loadAllstationradio,
} from "../../redux/actions/stationradioAction";
import {getStationradiosReselect} from "../../redux/selector"
import StationradioList from "./inc/StationradioList";


class StationradioIndexSite extends Component {

    componentDidMount() {
        this.props.loadAllstationradio(this.props);
    }

    render() {
        const {stationradios} = this.props;
        const mapStationradios = stationradios.length >= 0 ? (
            stationradios.map(item => {
                return (
                    <StationradioList key={item.id} {...item}  />
                )
            })
        ) : (
            <></>
        );
        return (
            <>
                <HelmetSite title={`Station radio | ${$name_site} `}/>

                <div className="ms_main_wrapper">

                    <VerticalNaveSite/>

                    <div className="ms_content_wrapper padder_top80">

                        <HorizontaNaveSite/>


                        { /**Banner**/}
                        <div className="ms-banner">

                            {/** Weekly radion 15**/}
                            <div className="ms_top_artist">
                                <div className="container-fluid">

                                    <UndoRedo {...this.props}/>

                                    <div className="row">
                                        <div className="col-lg-12">
                                            <div className="ms_heading">
                                                <h1>Live Stations</h1>
                                            </div>
                                        </div>

                                        {mapStationradios}

                                    </div>
                                </div>
                            </div>

                        </div>

                        <FooterMini/>

                        { /***Audio Player Section**/}



                    </div>

                </div>

                <FooterMultiple/>

            </>
        );
    }
}

StationradioIndexSite.propTypes = {
    loadAllstationradio: PropTypes.func.isRequired,
};

const mapStoreToProps = store => ({
    stationradios: getStationradiosReselect(store),
});

export default connect(mapStoreToProps, {
    loadAllstationradio,
})(StationradioIndexSite);

import React, {Component} from 'react';
import {Link} from "react-router-dom";
import PlayIcon from '../../../assets/images/svg/play.svg';
class StationradioList extends Component {
    render() {
        return (

            <div className="col-md-3 col-sm-3">
                <div className="ms_rcnt_box marger_bottom30">
                    <div className="ms_rcnt_box_img">
                        <Link to={`/radios/${this.props.slug}/`}><img src={this.props.photo} alt={this.props.name} className="img-fluid"/></Link>
                        <div className="ms_main_overlay">
                            <div className="ms_play_icon">
                                <Link to={`/radios/${this.props.slug}/`}><img src={PlayIcon} alt={this.props.name}/></Link>
                            </div>
                        </div>
                    </div>
                    <div className="ms_rcnt_box_text">
                        <h3><Link to={`/radios/${this.props.slug}/`}>{this.props.name} -  FM {this.props.station} </Link></h3>
                    </div>
                </div>
            </div>
        );
    }
}

export default StationradioList;

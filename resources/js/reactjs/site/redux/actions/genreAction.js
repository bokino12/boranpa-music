import {
    GET_ALL_GENRES,
    GET_SHOW_GENRE,
    GET_ALL_GENRES_INTERESSE,
} from "./index";

import Swal from "sweetalert2";


export const loadAlldata = (props) => dispatch => {

    let url = route('api.genres_site');
    dyaxios.get(url)
        .then(response => dispatch({
                type: GET_ALL_GENRES,
                payload: response.data
            })
        ).catch(error => console.error(error));
};

export const loadAlldatainter = () => dispatch => {

    let url = route('api.genres_inter_site');
    dyaxios.get(url)
        .then(response => dispatch({
                type: GET_ALL_GENRES_INTERESSE,
                payload: response.data
            })
        ).catch(error => console.error(error));
};

export const loadShowdata = (props) => dispatch => {

    let itemGenre = props.match.params.genre;
    let url = route('api.genres_show_site', [itemGenre]);
    dyaxios.get(url)
        .then(response => dispatch({
                type: GET_SHOW_GENRE,
                payload: response.data
            })
        ).catch(error => console.error(error));
};

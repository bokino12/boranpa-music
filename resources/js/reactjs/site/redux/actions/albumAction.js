import {
    GET_ALL_ALBUMS,
    GET_SHOW_ALBUM,
    GET_ALL_ALBUMS_INTERESSE,
    GET_ALL_ALBUMS_BY_USER_INTERESSE,
    FAVORITE_ALBUM_MUSIC_SHOW_ADD,
    FAVORITE_ALBUM_MUSIC_SHOW_REMOVE,
} from "./index";

import Swal from "sweetalert2";


export const loadAlldata = (props) => dispatch => {

    let url = route('api.genres_site');
    dyaxios.get(url)
        .then(response => dispatch({
                type: GET_ALL_ALBUMS,
                payload: response.data
            })
        ).catch(error => console.error(error));
};

export const loadAlldatainter = (props) => dispatch => {

    let url = route('api.albums_inter_site');
    dyaxios.get(url)
        .then(response => dispatch({
                type: GET_ALL_ALBUMS_INTERESSE,
                payload: response.data
            })
        ).catch(error => console.error(error));
};

export const loadAlldatainteruser = (props) => dispatch => {

    let itemUser = props.match.params.user;
    let url = route('api.albums_inter_user_site',[itemUser]);
    dyaxios.get(url)
        .then(response => dispatch({
                type: GET_ALL_ALBUMS_BY_USER_INTERESSE,
                payload: response.data
            })
        ).catch(error => console.error(error));
};

export const loadShowdata = (props) => dispatch => {

    let itemSlugin = props.match.params.music;
    let itemUser = props.match.params.user;
    let url = route('api.albums_show_site', [itemSlugin,itemUser]);
    dyaxios.get(url)
        .then(response => dispatch({
                type: GET_SHOW_ALBUM,
                payload: response.data
            })
        ).catch(error => console.error(error));
};

export const favoriteItem = props => dispatch => {

    const url = route('albums_favorites.favorite', [props.slugin]);
    dyaxios.post(url).then(() => {

        dispatch({
            type: FAVORITE_ALBUM_MUSIC_SHOW_ADD,
            payload: props.slugin
        });

    }).catch(error => console.error(error));
};

export const unfavoriteItem = props => dispatch => {

    const url = route('albums_favorites.unactive', [props.slugin]);
    dyaxios.post(url).then(() => {

        dispatch({
            type: FAVORITE_ALBUM_MUSIC_SHOW_REMOVE,
            payload: props.slugin
        });

    }).catch(error => console.error(error));
};


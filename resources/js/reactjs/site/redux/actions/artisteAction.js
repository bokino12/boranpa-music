import {
    GET_SHOW_ARTISTE,
    FAVORITE_ARTISTE_MUSIC_SHOW_ADD,
    FAVORITE_ARTISTE_MUSIC_SHOW_REMOVE,
} from "./index";

import Swal from "sweetalert2";


export const loadShowdata = (props) => dispatch => {

    let itemUser = props.match.params.user;
    let url = route('api.user_show_site', [itemUser]);
    dyaxios.get(url)
        .then(response => dispatch({
                type: GET_SHOW_ARTISTE,
                payload: response.data
            })
        ).catch(error => console.error(error));
};

export const favoriteItem = props => dispatch => {

    const url = route('users_favorites.favorite', [props.slug]);
    dyaxios.post(url).then(() => {

        dispatch({
            type: FAVORITE_ARTISTE_MUSIC_SHOW_ADD,
            payload: props.slug
        });

    }).catch(error => console.error(error));
};

export const unfavoriteItem = props => dispatch => {

    const url = route('users_favorites.unactive', [props.slug]);
    dyaxios.post(url).then(() => {

        dispatch({
            type: FAVORITE_ARTISTE_MUSIC_SHOW_REMOVE,
            payload: props.slug
        });

    }).catch(error => console.error(error));
};


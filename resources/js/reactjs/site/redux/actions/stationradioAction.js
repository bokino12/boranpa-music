import {
    GET_ALL_STATIONRADIONS,
    GET_SHOW_STATIONRADION,
    GET_ALL_STATIONRADIONS_INTERESSE,
    FAVORITE_STATIONRADIO_SHOW_ADD,
    FAVORITE_STATIONRADIO_SHOW_REMOVE,
} from "./index";

import Swal from "sweetalert2";


export const loadAllstationradio = (props) => dispatch => {

    let url = route('api.stationradio_site');
    dyaxios.get(url)
        .then(response => dispatch({
                type: GET_ALL_STATIONRADIONS,
                payload: response.data
            })
        ).catch(error => console.error(error));
};

export const loadAllstationradiointer = () => dispatch => {

    let url = route('api.stationradio_inter_site');
    dyaxios.get(url)
        .then(response => dispatch({
                type: GET_ALL_STATIONRADIONS_INTERESSE,
                payload: response.data
            })
        ).catch(error => console.error(error));
};

export const loadShowstationradio = (props) => dispatch => {

    let itemStationradio = props.match.params.stationradio;
    let url = route('api.stationradio_show_site', [itemStationradio]);
    dyaxios.get(url)
        .then(response => dispatch({
                type: GET_SHOW_STATIONRADION,
                payload: response.data
            })
        ).catch(error => console.error(error));
};

export const favoriteItem = props => dispatch => {

    const url = route('stationradios_favorites.favorite', [props.slugin]);
    dyaxios.post(url).then(() => {

        dispatch({
            type: FAVORITE_STATIONRADIO_SHOW_ADD,
            payload: props.slugin
        });

    }).catch(error => console.error(error));
};

export const unfavoriteItem = props => dispatch => {

    const url = route('stationradios_favorites.unactive', [props.slugin]);
    dyaxios.post(url).then(() => {

        dispatch({
            type: FAVORITE_STATIONRADIO_SHOW_REMOVE,
            payload: props.slugin
        });

    }).catch(error => console.error(error));
};

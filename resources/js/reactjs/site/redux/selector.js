import {createSelector} from "reselect";



const getGenres = (store) => store.genres.items;
const getGenre = (store) => store.genres.item;

export const getGenresReselect = createSelector([getGenres],
    (getGenres) => {
        return getGenres;
    });
export const getGenreReselect = createSelector([getGenre],
    (getGenre) => {
        return getGenre;
    });


const getAlbumsinter = (store) => store.albums.items;
const getAlbum = (store) => store.albums.item;

export const getAlbumsinterReselect = createSelector([getAlbumsinter],
    (getAlbumsinter) => {
        return getAlbumsinter;
    });
export const getAlbumReselect = createSelector([getAlbum],
    (getAlbum) => {
        return getAlbum;
    });

const getArtiste = (store) => store.artistes.item;
export const getArtisteReselect = createSelector([getArtiste],
    (getArtiste) => {
        return getArtiste;
    });


const getStationradios = (store) => store.stationradios.items;
const getStationradio = (store) => store.stationradios.item;

export const getStationradiosReselect = createSelector([getStationradios],
    (getStationradios) => {
        return getStationradios;
});
export const getStationradioReselect = createSelector([getStationradio],
    (getStationradio) => {
        return getStationradio;
});

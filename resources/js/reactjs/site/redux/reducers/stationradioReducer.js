import produce from "immer"


const initialState = {
    items: {},
    item: {},
};


export default produce((draft, action = {}) => {
        switch (action.type) {

            case 'GET_ALL_STATIONRADIONS':
                draft.items = action.payload;
                return;

            case 'GET_ALL_STATIONRADIONS_INTERESSE':
                draft.items = action.payload;
                return;

            case 'GET_SHOW_STATIONRADION':
                draft.item = action.payload;
                return;

            case 'FAVORITE_STATIONRADIO_SHOW_ADD':
                draft.item.favoriteted = action.payload;
                draft.item.countfavorites ++;
                return;

            case 'FAVORITE_STATIONRADIO_SHOW_REMOVE':
                draft.item.favoriteted = !action.payload;
                draft.item.countfavorites --;
                return;

        }
    },
    initialState
)

import {combineReducers} from "redux";
import stationradioReducer from "./stationradioReducer";
import genreReducer from "./genreReducer";
import albumReducer from "./albumReducer";
import artisteReducer from "./artisteReducer";



export default combineReducers({
    genres: genreReducer,
    albums: albumReducer,
    artistes: artisteReducer,
    stationradios: stationradioReducer,
})

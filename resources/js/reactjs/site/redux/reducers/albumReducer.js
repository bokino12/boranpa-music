import produce from "immer"


const initialState = {
    items: {user:[]},
    item: {user:[]},
};


export default produce((draft, action = {}) => {
        switch (action.type) {

            case 'GET_ALL_ALBUMS':
                draft.items = action.payload;
                return;

            case 'GET_ALL_ALBUMS_INTERESSE':
                draft.items = action.payload;
                return;

            case 'GET_ALL_ALBUMS_BY_USER_INTERESSE':
                draft.items = action.payload;
                return;

            case 'GET_SHOW_ALBUM':
                draft.item = action.payload;
                return;

            case 'FAVORITE_ALBUM_MUSIC_SHOW_ADD':
                draft.item.favoriteted = action.payload;
                draft.item.countfavorites ++;
                return;

            case 'FAVORITE_ALBUM_MUSIC_SHOW_REMOVE':
                draft.item.favoriteted = !action.payload;
                draft.item.countfavorites --;
                return;

        }
    },
    initialState
)

import produce from "immer"


const initialState = {
    items: {},
    item: {},
};


export default produce((draft, action = {}) => {
        switch (action.type) {

            case 'GET_ALL_GENRES':
                draft.items = action.payload;
                return;

            case 'GET_ALL_GENRES_INTERESSE':
                draft.items = action.payload;
                return;

            case 'GET_SHOW_GENRE':
                draft.item = action.payload;
                return;

        }
    },
    initialState
)

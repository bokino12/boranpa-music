import produce from "immer"


const initialState = {
    item: {user:[]},
};


export default produce((draft, action = {}) => {
        switch (action.type) {

            case 'GET_SHOW_ARTISTE':
                draft.item = action.payload;
                return;

            case 'FAVORITE_ARTISTE_MUSIC_SHOW_ADD':
                draft.item.favoriteted = action.payload;
                draft.item.countfavorites ++;
                return;

            case 'FAVORITE_ARTISTE_MUSIC_SHOW_REMOVE':
                draft.item.favoriteted = !action.payload;
                draft.item.countfavorites --;
                return;
        }
    },
    initialState
)

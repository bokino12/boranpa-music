import React, { Fragment, Suspense } from 'react';
import { Route, Switch, withRouter } from 'react-router-dom';
import IndexSite from "../components/IndexSite";
import FavoriteSite from "../components/private/FavoriteSite";
import AlbumShowSite from "../components/albums/AlbumShowSite";
import StatisticCountrySite from "../components/statistic/StatisticCountrySite";
import StationradioIndexSite from "../components/stationradio/StationradioIndexSite";
import AlbumIndexSite from "../components/albums/AlbumIndexSite";
import ArtisteIndexSite from "../components/artiste/ArtisteIndexSite";
import GenresIndexSite from "../components/genres/GenresIndexSite";
import DasborbIndexSite from "../components/DasborbIndexSite";
import ArtisteShowSite from "../components/artiste/ArtisteShowSite";
import GenresShowSite from "../components/genres/GenresShowSite";
import StationradioShowSite from "../components/stationradio/StationradioShowSite";



const RouterUser = () => (

    <Fragment>
        <Suspense fallback={"wait"}>
            <Switch>
                <Route exact path="/" component={IndexSite} />
                <Route exact path="/home/" component={DasborbIndexSite} />
                <Route exact path="/favorites/" component={FavoriteSite} />
                <Route exact path="/artistes/" component={ArtisteIndexSite} />
                <Route exact path="/statistics/country/" component={StatisticCountrySite} />


                <Route exact path="/albums/" component={AlbumIndexSite} />


                <Route exact path="/user/:user/" component={ArtisteShowSite} />
                <Route exact path="/albums/:music/:user/" component={withRouter(AlbumShowSite)} />
                <Route exact path="/genres/" component={GenresIndexSite} />
                <Route exact path="/genres/:genre/" component={withRouter(GenresShowSite)} />
                <Route exact path="/radios/" component={StationradioIndexSite} />
                <Route exact path="/radios/:stationradio/" component={withRouter(StationradioShowSite)} />

            </Switch>
        </Suspense>
    </Fragment>

);
export default RouterUser;

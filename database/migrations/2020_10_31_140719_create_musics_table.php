<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateMusicsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('musics', function (Blueprint $table) {
            $table->id();
            $table->string('name')->nullable();
            $table->string('slug')->nullable();
            $table->string('ip')->nullable();
            $table->string('slugin')->nullable();
            $table->text('artiste_name')->nullable();
            $table->string('agence_production')->nullable();
            $table->string('photo')->nullable();
            $table->boolean('status')->default(true);
            $table->boolean('status_admin')->default(false);
            $table->longText('description')->nullable();
            $table->bigInteger('category')->nullable();
            $table->bigInteger('year_production')->nullable();
            $table->timestamps();

            $table->unsignedBigInteger('user_id')->nullable()->index();
            $table->foreign('user_id')->references('id')->on('users')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('musics');
    }
}

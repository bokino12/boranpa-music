<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateStationradiosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('stationradios', function (Blueprint $table) {
            $table->id();
            $table->string('name')->nullable();
            $table->string('label')->nullable();
            $table->boolean('status')->default(false);
            $table->unsignedBigInteger('station')->nullable();
            $table->string('slug')->nullable();
            $table->string('slugin')->nullable();
            $table->string('city')->nullable();
            $table->longText('description')->nullable();
            $table->text('photo')->nullable();
            $table->string('link_red')->nullable();
            $table->string('ip')->nullable();
            $table->unsignedBigInteger('user_id')->nullable()->index();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('stationradios');
    }
}

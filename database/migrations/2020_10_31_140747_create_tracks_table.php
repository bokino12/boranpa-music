<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTracksTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tracks', function (Blueprint $table) {
            $table->id();
            $table->string('title')->nullable();
            $table->string('ip')->nullable();
            $table->text('link_tack_mp3')->nullable();
            $table->text('link_tack_flac')->nullable();
            $table->text('link_tack_ogg')->nullable();
            $table->text('photo')->nullable();
            $table->bigInteger('year_production')->nullable();
            $table->bigInteger('genre_id')->nullable()->index();
            $table->boolean('status')->default(true);
            $table->boolean('status_admin')->default(true);
            $table->string('trackable_type')->nullable();
            $table->unsignedBigInteger('trackable_id')->nullable();
            $table->timestamps();

            $table->unsignedBigInteger('user_id')->nullable()->index();
            $table->foreign('user_id')->references('id')->on('users')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tracks');
    }
}

<?php

namespace Database\Factories;

use App\Models\favorite;
use App\Models\music;
use App\Models\stationradio;
use App\Models\track;
use App\Models\user;
use Illuminate\Database\Eloquent\Factories\Factory;

class favoriteFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = favorite::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        $servicemodel = collect([
            ['name' => stationradio::class],
            ['name' => music::class],
            ['name' => track::class],
        ]);

        return [
            'user_id' => user::inRandomOrder()->first()->id,
            'favoriteable_id' => mt_rand(1, 30),
            'favoriteable_type' => $servicemodel->shuffle()->first()['name'],
        ];
    }
}

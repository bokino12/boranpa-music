<?php

namespace Database\Factories;

use App\Models\stationradio;
use App\Models\user;
use Illuminate\Database\Eloquent\Factories\Factory;
use Illuminate\Support\Str;

class stationradioFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = stationradio::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        $name = $this->faker->userName;
        $servicemodel = collect([
            ['name' => '/assets/images/radio/img'],
            ['name' => '/assets/images/album/album'],
        ]);
        return [
            'name' => $name,
            'label' => $name,
            'status' => $this->faker->boolean,
            'slugin' => Str::uuid(),
            'link_red' => "http://mbindi-radio.com/",
            'city' => $this->faker->country,
            'description' => $this->faker->realText(rand(10, 400)),
            'photo' =>  $servicemodel->shuffle()->first()['name'].mt_rand(1,10).".jpg",
            'user_id' => user::inRandomOrder()->first()->id,
            'slug' => str_slug($name),
            'station' => mt_rand(100,199),
        ];
    }
}

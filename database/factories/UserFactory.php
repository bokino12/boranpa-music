<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Models\country;
use App\Models\user;
use Faker\Generator as Faker;
use Illuminate\Support\Str;

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| This directory should contain each of the model factory definitions for
| your application. Factories provide a convenient way to generate new
| model instances for testing / seeding your application's database.
|
*/

$factory->define(user::class, function (Faker $faker) {
    $coveravatar = collect([
        ['name' => '/assets/images/radio/img'],
        ['name' => '/assets/images/album/album'],
    ]);
    $avatar = collect([
        ['name' => '/assets/images/artist/artist'],
    ]);

    $username = $faker->userName;
    return [
        'name' => $faker->name,
        'city_name' => $faker->city,
        'username' => $username,
        'slug' => str_slug($username),
        'description' => $faker->realText(rand(10, 400)),
        'email' => $faker->unique()->safeEmail,
        'avatar' =>  $avatar->shuffle()->first()['name'].mt_rand(1,10).".jpg",
        'avatar_cover' =>  $coveravatar->shuffle()->first()['name'].mt_rand(1,10).".jpg",
        'country_id' => country::inRandomOrder()->first()->id,
        'email_verified_at' => now(),
        'password' => '$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi', // password
        'remember_token' => Str::random(10),
    ];
});

<?php

namespace Database\Factories;

use App\Models\genre;
use App\Models\track;
use App\Models\user;
use Faker\Generator as Faker;
use App\Models\music;

$factory->define(track::class, function (Faker $faker) {
    $title = $faker->name;
    $linktrack = collect([
        ['name' => 'http://www.jplayer.org/audio/mp3/TSP-01-Cro_magnon_man'],
        ['name' => 'http://www.jplayer.org/audio/mp3/TSP-05-Your_face'],
        ['name' => 'http://www.jplayer.org/audio/mp3/TSP-05-Your_face'],
        ['name' => 'http://www.jplayer.org/audio/mp3/TSP-07-Cybersonnet'],
        ['name' => 'http://www.jplayer.org/audio/mp3/Miaow-01-Tempered-song'],
        ['name' => 'http://www.jplayer.org/audio/mp3/Miaow-02-Hidden'],
        ['name' => 'http://www.jplayer.org/audio/mp3/Miaow-03-Lentement'],
        ['name' => 'http://www.jplayer.org/audio/mp3/Miaow-04-Lismore'],
        ['name' => 'http://www.jplayer.org/audio/mp3/Miaow-05-The-separation'],
        ['name' => 'http://www.jplayer.org/audio/mp3/Miaow-06-Beside-me'],
        ['name' => 'http://www.jplayer.org/audio/mp3/Miaow-07-Bubble'],
    ]);

    $servicemodel = collect([
        ['name' => music::class],
        //['name' => postcast::class], // In the futur time
    ]);

    $linkmytrack = $linktrack->shuffle()->first()['name'];

    return [
        'title' => $title,
        'status_admin' => $faker->boolean,
        'ip' => $faker->ipv4,
        'year_production' => mt_rand(1990, 2021),
        'genre_id' => genre::inRandomOrder()->first()->id,
        'photo' => music::inRandomOrder()->first()->photo,
        'link_tack_mp3' =>  $linkmytrack.".mp3",
        'link_tack_flac' =>  $linkmytrack.".flac",
        'link_tack_ogg' =>  $linkmytrack.".ogg",
        'user_id' => user::inRandomOrder()->first()->id,
        'trackable_id' => mt_rand(1, 100),
        'trackable_type' => $servicemodel->shuffle()->first()['name'],
    ];
});

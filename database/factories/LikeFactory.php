<?php

namespace Database\Factories;

use App\Models\like;
use App\Models\stationradio;
use App\Models\track;
use App\Models\user;
use Illuminate\Database\Eloquent\Factories\Factory;

class likeFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = like::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        $servicemodel = collect([
            ['name' => stationradio::class],
            ['name' => track::class],
        ]);

        return [
            'user_id' => user::inRandomOrder()->first()->id,
            'likeable_id' => mt_rand(1, 30),
            'likeable_type' => $servicemodel->shuffle()->first()['name'],
        ];
    }
}

<?php

namespace Database\Factories;

use App\Models\genre;
use App\Models\user;
use Faker\Generator as Faker;
use Illuminate\Support\Str;


/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| This directory should contain each of the model factory definitions for
| your application. Factories provide a convenient way to generate new
| model instances for testing / seeding your application's database.
|
*/

$factory->define(genre::class, function (Faker $faker) {
    $name = $faker->userName;
    $servicemodel = collect([
        ['name' => '/assets/images/radio/img'],
        ['name' => '/assets/images/album/album'],
    ]);
    return [
        'name' => $name,
        'label' => $name,
        'slug' => str_slug($name),
        'status' => $faker->boolean,
        'slugin' => Str::uuid(),
        'description' => $this->faker->realText(rand(10, 400)),
        'photo' =>  $servicemodel->shuffle()->first()['name'].mt_rand(1,10).".jpg",
        'user_id' => user::inRandomOrder()->first()->id,
    ];
});

<?php

namespace Database\Factories;

use App\Models\music;
use App\Models\user;
use Faker\Generator as Faker;
use Illuminate\Support\Str;


/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| This directory should contain each of the model factory definitions for
| your application. Factories provide a convenient way to generate new
| model instances for testing / seeding your application's database.
|
*/

$factory->define(music::class, function (Faker $faker) {
    $name = $faker->userName;
    $servicemodel = collect([
        ['name' => '/assets/images/radio/img'],
        ['name' => '/assets/images/album/album'],
    ]);
    return [
        'name' => $name,
        'agence_production' => $faker->firstName,
        'slug' => str_slug($name),
        'slugin' => sha1(('YmdHis') . str_random(4)),
        'status' => true,
        'status_admin' => true,
        'ip' => $faker->ipv4,
        'year_production' => mt_rand(1990, 2021),
        'category' => mt_rand(1, 2), // 1 => single; 2 => album
        'description' => $this->faker->realText(rand(10, 400)),
        'photo' =>  $servicemodel->shuffle()->first()['name'].mt_rand(1,10).".jpg",
        'user_id' => user::inRandomOrder()->first()->id,
    ];
});

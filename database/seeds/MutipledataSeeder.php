<?php

use App\Models\genre;
use App\Models\like;
use App\Models\stationradio;
use App\Models\track;
use App\Models\music;
use App\Models\user;
use Illuminate\Database\Seeder;

class MutipledataSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        factory(user::class, 6)->create();
        factory(genre::class, 40)->create();
        factory(music::class, 160)->create();
        factory(track::class, 8000)->create();
        stationradio::factory()->count(40)->create();
        like::factory()->count(890)->create();
        like::factory()->count(890)->create();

    }
}

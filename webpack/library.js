const mix = require('laravel-mix');

module.exports = output => {

    const path_from = './resources/js';

    mix.react(path_from + '/library.js', output + '/js/site')
        .sourceMaps();
};

<?php

namespace App\Http\Resources;

use App\Models\stationradio;
use Illuminate\Http\Resources\Json\JsonResource;

class StationradioResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'name' => $this->name,
            'label' => $this->label,
            'status' => $this->status,
            'station' => $this->station,
            'slug' => $this->slug,
            'likeked' => $this->likeked(),
            'favoriteted' => $this->favoriteted(),
            'countfavorites' => $this->favorites()
                ->whereIn('favoriteable_id',[$this->id])
                ->where('favoriteable_type', stationradio::class)
                ->count(),
            'slugin' => $this->slugin,
            'city' => $this->city,
            'description' => $this->description,
            'link_red' => $this->link_red,
            'photo' => $this->photo,
            'photo_cover' => $this->photo_cover,
            'created_at' => (string) $this->created_at,
            'updated_at' => (string) $this->updated_at,
        ];
    }
}

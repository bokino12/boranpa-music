<?php

namespace App\Http\Resources;

use App\Models\music;
use Illuminate\Http\Resources\Json\JsonResource;

class MusicResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'name' => $this->name,
            'slug' => $this->slug,
            'status' => $this->status,
            'artiste_name' => $this->artiste_name,
            'agence_production' => $this->agence_production,
            'likeked' => $this->likeked(),
            'favoriteted' => $this->favoriteted(),
            'countfavorites' => $this->favorites()
                ->whereIn('favoriteable_id',[$this->id])
                ->where('favoriteable_type', music::class)
                ->count(),
            'slugin' => $this->slugin,
            'status_admin' => $this->status_admin,
            'category' => $this->category,
            'year_production' => $this->year_production,
            'user_id' => $this->user_id,
            'user' => $this->user,
            'tracks_count' => $this->tracks_count,
            'description' => $this->description,
            'photo' => $this->photo,
            'created_at' => (string) $this->created_at,
            'updated_at' => (string) $this->updated_at,
        ];
    }
}

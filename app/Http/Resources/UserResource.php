<?php

namespace App\Http\Resources;

use App\Models\user;
use Illuminate\Http\Resources\Json\JsonResource;

class UserResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'name' => $this->name,
            'username' => $this->username,
            'slug' => $this->slug,
            'country_id' => $this->country_id,
            'country' => $this->country,
            'favoriteted' => $this->favoriteted(),
            'countfavorites' => $this->favorites()
                ->whereIn('favoriteable_id',[$this->id])
                ->where('favoriteable_type', user::class)
                ->count(),
            'email' => $this->email,
            'city_name' => $this->city_name,
            'avatar' => $this->avatar,
            'description' => $this->description,
            'avatar_cover' => $this->avatar_cover,
        ];
    }
}

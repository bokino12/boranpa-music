<?php

namespace App\Http\Controllers\User;

use App\Http\Controllers\Controller;
use App\Models\genre;
use Illuminate\Http\Request;

class GenreController extends Controller
{


    public function apiForSite()
    {
        $data = genre::where(['status' => 1])->orderByDesc('created_at')->get();

        return response()->json($data, 200);
    }

    public function apiInterForSite()
    {
        $data = genre::where(['status' => 1])->orderByDesc('name')->take(4)->get();

        return response()->json($data, 200);
    }

    public function apiShowForSite(genre $genre)
    {
        $data = genre::whereSlug($genre->slug)->where(['status' => 1])->first();

        return response()->json($data, 200);
    }

    public function index()
    {
        return view('site.genre.index');
    }

    public function show(genre $genre)
    {
        return view('site.genre.show',compact('genre'));
    }
}

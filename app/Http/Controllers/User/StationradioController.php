<?php

namespace App\Http\Controllers\User;

use App\Http\Controllers\Controller;
use App\Http\Resources\StationradioResource;
use App\Models\stationradio;
use Illuminate\Http\Request;

class StationradioController extends Controller
{


    public function apiForSite()
    {
        $data = StationradioResource::collection(

            stationradio::where(['status' => 1])->orderByDesc('created_at')->get()
        );

        return response()->json($data, 200);
    }

    public function apiInterForSite()
    {
        $data = StationradioResource::collection(

            stationradio::where(['status' => 1])->orderByDesc('city')->take(4)->get()
        );

        return response()->json($data, 200);
    }

    public function apiShowForSite(stationradio $stationradio)
    {
        $data = new StationradioResource(stationradio::whereSlug($stationradio->slug)->where(['status' => 1])->first());

        return response()->json($data, 200);
    }

    public function index()
    {
        return view('site.stationradio.index');
    }

    public function show(stationradio $stationradio)
    {
        return view('site.stationradio.show',compact('stationradio'));
    }
}

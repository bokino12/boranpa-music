<?php

namespace App\Http\Controllers\User;

use App\Http\Controllers\Controller;
use App\Http\Resources\StationradioResource;
use App\Models\favorite;
use App\Models\music;
use App\Models\stationradio;
use App\Models\user;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class FavoriteController extends Controller
{


    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }


    public function index()
    {
        $user = Auth::user();
        return view('site.favorite.show',compact('user'));
    }

    public function favoritestationradio(Request $request,stationradio $stationradio)
    {
        $response = $stationradio->favorites()->create($request->all());

        return response()->json(['success'=>$response]);

    }

    public function unfavoritestationradio(stationradio $stationradio)
    {
        $response = auth()->user()->removefavorites()->detach($stationradio);

        return response()->json(['success'=>$response]);
    }

    public function favoritealbum(Request $request,music $music)
    {
        $response = $music->favorites()->create($request->all());

        return response()->json(['success'=>$response]);
    }

    public function unfavoritealbum(music $music)
    {
        $response = auth()->user()->removefavorites()->detach($music);

        return response()->json(['success'=>$response]);
    }

    public function favoriteuser(Request $request,user $user)
    {
        $response = $user->favorites()->create($request->all());

        return response()->json(['success'=>$response]);
    }

    public function unfavoriteuser(user $user)
    {
        $response = auth()->user()->removefavorites()->detach($user);

        return response()->json(['success'=>$response]);
    }

    public function destroy(favorite $favorite)
    {
        $user = Auth::user();
        $this->authorize('update',$user);

        $favorite->delete();
    }
}

<?php

namespace App\Http\Controllers\User;

use App\Http\Controllers\Controller;
use App\Models\music;
use App\Models\user;
use Illuminate\Http\Request;

class TrackController extends Controller
{



    public function apiShowForSite(music $music,$user)
    {
       $myuser = user::whereSlug($user)->first();
        $data = $music->tracks()
            ->with('user')
            ->whereIn('user_id',[$myuser->id])
            ->whereIn('trackable_id',[$music->id])
            ->where('trackable_type',music::class)
            ->trackactive()
            ->orderByDesc('created_at')->distinct()->get();

        return response()->json($data, 200);
    }

}

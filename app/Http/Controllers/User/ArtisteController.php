<?php

namespace App\Http\Controllers\User;

use App\Http\Controllers\Controller;
use App\Http\Resources\MusicResource;
use App\Http\Resources\UserResource;
use App\Models\music;
use App\Models\user;
use Illuminate\Http\Request;

class ArtisteController extends Controller
{


    public function apiForSite()
    {

    }

    public function apiShowForSite(user $user)
    {
        $data = new UserResource(user::whereSlug($user->slug)
            ->with('country')->first());

        return response()->json($data, 200);
    }

    public function index()
    {
        return view('site.artiste.index');
    }

    public function show(user $user)
    {
        return view('site.artiste.show',compact('user'));
    }
}

<?php

namespace App\Http\Controllers\User;

use App\Http\Controllers\Controller;
use App\Http\Resources\MusicResource;
use App\Models\music;
use App\Models\user;
use Illuminate\Http\Request;

class AlbumController extends Controller
{


    public function apiForSite()
    {
        $data = music::albumactive()
            ->with('user')
            ->orderByDesc('created_at')->get();

        return response()->json($data, 200);
    }

    /*Avec la fuction ci dessou je recupere les album **/

    public function apiInterForSite()
    {
        $data = music::albumactive()
            ->with('user')->orderByDesc('created_at')
            ->take(4)->get();

        return response()->json($data, 200);
    }

    public function apiInterUserForSite(user $user)
    {
        $data = $user->musics()->albumactive()
            ->with('user')->orderByDesc('year_production')
            ->get();

        return response()->json($data, 200);
    }

    public function apiShowForSite(music $music,$user)
    {
        $myuser = user::whereSlug($user)->first();

        $data = new MusicResource(music::whereSlugin($music->slugin)
            ->with('user')
            ->withCount(['tracks' => function ($q) use ($music,$myuser) {
                $q->trackactive()
                    ->whereIn('user_id',[$myuser->id])
                    ->whereIn('trackable_id',[$music->id])
                    ->where('trackable_type',music::class);
            }])
            ->where(['status' => 1, 'status_admin' => 1])
            ->first());

        return response()->json($data, 200);
    }

    public function index()
    {
        return view('site.music.index');
    }

    public function show(music $music,$user)
    {
        return view('site.music.show',compact('music'));
    }
}

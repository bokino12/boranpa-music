<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Auth;

class music extends Model
{
    protected $guarded = [];

    protected $table = "musics";

    public function user()
    {
        return $this->belongsTo(user::class,'user_id');
    }

    protected $casts = [
        'status' => 'boolean',
        'status_admin' => 'boolean',
    ];

    protected static function boot()
    {
        parent::boot();

        static::creating(function ($model){
            if (auth()->check()){
                $model->user_id = auth()->id();
                $model->ip = request()->ip();
                $model->slugin =  sha1(('YmdHis') . str_random(4));
            }
        });

        static::updating(function($model){
            $model->ip = request()->ip();
        });
    }

    public function tracks()
    {
        return $this->morphMany(track::class ,'trackable');
    }

    public function likes()
    {
        return $this->morphMany(like::class ,'likeable');
    }

    public function likeked()
    {
        return (bool) like::where('user_id', Auth::guard('web')->id())
            ->where(['likeable_type' => music::class,
                'likeable_id' => $this->id ])
            ->first();
    }

    public function favorites()
    {
        return $this->morphMany(favorite::class ,'favoriteable');
    }

    public function favoriteted()
    {
        return (bool) favorite::where('user_id', Auth::id())
            ->where(['favoriteable_type' => music::class,
                'favoriteable_id' => $this->id ])
            ->first();
    }
    /**
     * Scope a query to only include active album.
     */
    public function scopeAlbumactive($q)
    {
        return $q->where(['status' => 1, 'status_admin' => 1, 'category' => 2]);
    }
}

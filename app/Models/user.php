<?php

namespace App\Models;

use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Illuminate\Support\Facades\Auth;
use Spatie\Permission\Traits\HasRoles;

class user extends Authenticatable
{
    use Notifiable,HasRoles;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    public function country()
    {
        return $this->belongsTo(country::class,'country_id');
    }

    public function removelikes()
    {
        return $this->belongsToMany(
            like::class,
            'likes',
            'user_id',
            'likeable_id')
            ->withTimeStamps();
    }

    public function removefavorites()
    {
        return $this->belongsToMany(
            favorite::class,
            'favorites',
            'user_id',
            'favoriteable_id')
            ->withTimeStamps();
    }

    public function musics()
    {
        return $this->hasMany(music::class,'user_id');
    }

    public function favorites()
    {
        return $this->morphMany(favorite::class ,'favoriteable');
    }

    public function favoriteted()
    {
        return (bool) favorite::where('user_id', Auth::id())
            ->where(['favoriteable_type' => user::class,
                'favoriteable_id' => $this->id ])
            ->first();
    }
}

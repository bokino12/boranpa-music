<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class track extends Model
{
    protected $guarded = [];

    protected $table = "tracks";

    public function user()
    {
        return $this->belongsTo(user::class,'user_id');
    }

    public function genre()
    {
        return $this->belongsTo(genre::class,'genre_id');
    }

    protected $casts = [
        'status' => 'boolean',
        'status_admin' => 'boolean',
    ];

    protected static function boot()
    {
        parent::boot();

        static::creating(function ($model){
            if (auth()->check()){
                $model->user_id = auth()->id();
                $model->ip = request()->ip();
                $model->slugin =  sha1(('YmdHis') . str_random(4));
            }
        });

        static::updating(function($model){
            $model->ip = request()->ip();
        });
    }

    public function trackable()
    {
        return $this->morphTo();
    }

    /**
     * Scope a query to only include active album.
     */
    public function scopeTrackactive($q)
    {
        return $q->where(['status' => 1, 'status_admin' => 1]);
    }
}

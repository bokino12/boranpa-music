const mix = require('laravel-mix');
const compile_react = require('./webpack/react');


const output = 'public';
compile_react(output);



if (mix.inProduction()) {
    mix.version();
}

<?php

use App\Http\Controllers\User\FavoriteController;

Route::group(['prefix' => 'api'], function () {



});

Route::get(
    'favorites',
    [FavoriteController::class, 'index']
)->name('favorites.index');

Route::post(
    'radios/{stationradio:slugin}/favorite',
    [FavoriteController::class, 'favoritestationradio']
)->name('stationradios_favorites.favorite');

Route::post(
    'radios/{stationradio:slugin}/unfavorites',
    [FavoriteController::class, 'unfavoritestationradio']
)->name('stationradios_favorites.unactive');

Route::post(
    'albums/{music:slugin}/favorite',
    [FavoriteController::class, 'favoritealbum']
)->name('albums_favorites.favorite');

Route::post(
    'albums/{music:slugin}/unfavorites',
    [FavoriteController::class, 'unfavoritealbum']
)->name('albums_favorites.unactive');

Route::post(
    'user/{user:slug}/favorite',
    [FavoriteController::class, 'favoriteuser']
)->name('users_favorites.favorite');

Route::post(
    'user/{user:slug}/unfavorites',
    [FavoriteController::class, 'unfavoriteuser']
)->name('users_favorites.unactive');


<?php

use App\Http\Controllers\User\StationradioController;

Route::group(['prefix' => 'api'], function () {

    Route::get(
        'radios',
        [StationradioController::class, 'apiForSite']
    )->name('api.stationradio_site');

    Route::get(
        'radios_inter',
        [StationradioController::class, 'apiInterForSite']
    )->name('api.stationradio_inter_site');

    Route::get(
        'radios/{stationradio:slug}',
        [StationradioController::class, 'apiShowForSite']
    )->name('api.stationradio_show_site');

});

Route::get(
    'radios',
    [StationradioController::class, 'index']
)->name('stationradio_site');

Route::get(
    'radios/{stationradio:slug}',
    [StationradioController::class, 'show']
)->name('stationradio_show_site');


<?php

use App\Http\Controllers\User\ArtisteController;

Route::group(['prefix' => 'api'], function () {

    Route::get(
        'users',
        [ArtisteController::class, 'apiForSite']
    )->name('api.users_site');

    Route::get(
        'user/{user:slug}',
        [ArtisteController::class, 'apiShowForSite']
    )->name('api.user_show_site');

});

Route::get(
    'users',
    [ArtisteController::class, 'index']
)->name('users_site');

Route::get(
    'user/{user:slug}',
    [ArtisteController::class, 'show']
)->name('user_show_site');


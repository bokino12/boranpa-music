<?php

use App\Http\Controllers\User\TrackController;

Route::group(['prefix' => 'api'], function () {

    Route::get(
        'albums/{music:slugin}/{user:slug}/tracks',
        [TrackController::class, 'apiShowForSite']
    )->name('api.tracks_show_site');

});



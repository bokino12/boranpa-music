<?php

use App\Http\Controllers\User\AlbumController;

Route::group(['prefix' => 'api'], function () {

    Route::get(
        'albums',
        [AlbumController::class, 'apiForSite']
    )->name('api.albums_site');

    Route::get(
        'albums_inter',
        [AlbumController::class, 'apiInterForSite']
    )->name('api.albums_inter_site');

    Route::get(
        'albums_inter/{user:slug}',
        [AlbumController::class, 'apiInterUserForSite']
    )->name('api.albums_inter_user_site');

    Route::get(
        'albums/{music:slugin}/{user:slug}',
        [AlbumController::class, 'apiShowForSite']
    )->name('api.albums_show_site');

});

Route::get(
    'albums',
    [AlbumController::class, 'index']
)->name('albums_site');

Route::get(
    'albums/{music:slugin}/{user:slug}',
    [AlbumController::class, 'show']
)->name('albums_show_site');


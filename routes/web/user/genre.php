<?php

use App\Http\Controllers\User\GenreController;

Route::group(['prefix' => 'api'], function () {

    Route::get(
        'genres',
        [GenreController::class, 'apiForSite']
    )->name('api.genres_site');

    Route::get(
        'genres_inter',
        [GenreController::class, 'apiInterForSite']
    )->name('api.genres_inter_site');

    Route::get(
        'genres/{genre:slug}',
        [GenreController::class, 'apiShowForSite']
    )->name('api.genres_show_site');

});

Route::get(
    'genres',
    [GenreController::class, 'index']
)->name('genres_site');

Route::get(
    'genres/{genre:slug}',
    [GenreController::class, 'show']
)->name('genres_show_site');

